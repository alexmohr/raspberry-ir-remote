# Protocol
This file describes tde protocol which is used to interact with the server. <br>
You can easily examine them by running vidar-remote from your installation folder.

<br><br>
<table> 
	<tr>
		<th>
			Command
		</th>
		<th>
			Result
		</th>
		<th>
			Description
		</th>
	</tr>
	<tr>
		<td>
			`{"deivce": "NAME", "key": "KEY_CODE"}`
		</td>
		<td>
			{'state': 'RETURN OF COMMAND'}
		</td>
		<td>
			Sends the given key to the device.
		</td>
	<tr>
	<tr>
		<td>
			`{"reload": true}`
		</td>
		<td>
			{'state': true}
		</td>
		<td>
			Reloads the vidar configuratiob 
		</td>
	<tr>
	<tr>
		<td> 
			`{"listdevices": true}` <br>
			or <br>
			`{"listdevices": true, device:"DEVICE"}` <br>
		</td>
		<td>
			`[<br>
				{'name': 'Marantz', 'keys': <br>
					[<br>
						{'name': 'KEY_POWER', 'code': '<raw>'}, <br>
					 	{'name': 'KEY_SAT', 'code': '<raw>'}, <br>
				 	]<br>
			 	},<br>
			 	{'name': 'TV', 'keys': <br>
			 		[<br>
			 			{'name': 'KEY_POWER', 'code': '0x40BF'}<br>
			 		]<br>
				}<br>
			]<br>`

			`
		</td>
		<td>
			Lists all devices with all supported keys. If a name of a device is given only the keys of this device will be transmitted.

		</td>
	<tr>
</table>
