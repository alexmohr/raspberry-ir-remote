# Plugins
Vidar supports the integration of plugins to include new devices.<br>
All devices are realised as plugin. 

# Configuration of plugins
The file `plugin_config.json` is a directory to the configuration files of the plugins.
Take a look at this file to find out where the plugins can be configured.

# How to create a plugin
Follow these step to create a new Vidar plugin.
Note that you have to replace `yourplugin` with the actual plugin name. <br>
<br>
* Create a new file named `yourplugin` or `yourplugin.py`
* Optional create a class in your module.
* Implement base class pluginbase with the methods: 
    * get_devices(self):devices[] - This returns an instance of the device class filled with the keys the device supports
    * handle_key(self, key, value, device):String - Handles the given key and returns the result of the key.
    * configure_plugin(self, file): void - Parses the given configuration file. The file can be configured via the plugin config json.
* Add your plugin to the configuration json file. If you are not using a class just leave the class part out.
