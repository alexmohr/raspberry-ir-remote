## Used resources

http://alexba.in/blog/2013/01/06/setting-up-lirc-on-the-raspberrypi/

## Used Hardware 

*Raspberry*
https://www.amazon.de/Raspberry-Pi-3-Model-B/dp/B01CEFWQFA/ref=sr_1_4?ie=UTF8&qid=1464271654&sr=8-4&keywords=raspberry+pi+2

*LEDS*
https://de.aliexpress.com/item/10pcs-LED-3mm-940nm-IR-Infrared-Emitting-Round-Tube-Light-diode/32432134646.html?ws_ab_test=searchweb201556_10,searchweb201602_3_10017_10021_507_10022_10020_10009_10008_10018_10019_101,searchweb201603_9&btsid=3fe6e0d5-022b-43b7-8766-0d220e660060

https://de.aliexpress.com/item/10pcs-LED-5mm-940nm-IR-Infrared-Emitting-Round-Tube-Light-diode/32432178991.html?ws_ab_test=searchweb201556_10,searchweb201602_3_10017_10021_507_10022_10020_10009_10008_10018_10019_101,searchweb201603_9&btsid=b7bf41dc-d950-40c2-8c37-e51a31f68941

*IR receiver*
https://www.adafruit.com/products/157

*Transitors*
https://de.aliexpress.com/item/10PCS-Transistor-PN2222A-PN2222-TO-92-NPN/32558133124.html?spm=2114.010208.3.11.y2UYWC&ws_ab_test=searchweb201556_10,searchweb201602_3_10017_10021_507_10022_10020_10009_10008_10018_10019_101,searchweb201603_9&btsid=a2b192d2-0a62-4821-85fc-dbf65d54ee67

*Other*
https://www.amazon.de/Solder-Finished-Prototype-Circuit-Breadboard/dp/B00FXHXT80/ref=sr_1_6?ie=UTF8&qid=1464272214&sr=8-6&keywords=prototype+board

10k ohm resitor 

## Configuration
List all keycodes;
`irrecord --list-namespace`


