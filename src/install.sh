#!/bin/sh

# Variables
binDir=/opt/vidar

# Create binary folder in opt
mkdir -p $binDir

if [ ! -w "$binDir" ]
  then echo 'Try running as root there is no access to direcoty:' $binDir
  exit 113 
 fi

# Copy data to folder 
cp -r cfg $binDir
cp -r frontend $binDir
cp -r server $binDir 

lessc --clean-css frontend/css/style.less $binDir/frontend/css/style.css 

# Install systemd scripts
cp systemd/vidard /usr/bin/vidard
chmod +x /usr/bin/vidard

cp systemd/vidar.service /etc/systemd/system/
systemctl enable vidar.service

# overwrite the configuraiton
# todo insert warning
cat cfg/lirc/lircd.conf.d/marantz.conf > /etc/lirc/lircd.conf
cat cfg/lirc/lircd.conf.d/tv.conf >> /etc/lirc/lircd.conf
cat cfg/lirc/lircd.conf.d/bluray.conf >> /etc/lirc/lircd.conf

# restart services
systemctl restart lirc
systemctl restart vidar
