# JSON Description
This file descripes for what the different json file are used. 

## Plugin configuration files
Each plugin configuration file is named like the plugin class itself. 
It will be loaded automitcally when the plugin is loaded. 

## lirc.json
Contains configuration information from lirc. 
It defines additional information aside from the configuration which 
is included in the lirc configuration file. 
This includes information for vidar like how the configuration file of lirc is parsed and additional information
which can be used to enrich a device with information. 
An example for this could be an icon. 

### Nonparsable descripted json
{
	// options for lirc. 
	"lirc":{
		// the path to the lirc configuration file. 
		"config":"/etc/lirc/lircd.conf"
	}, 
	// Devices, providing addtional information about them 
	"devices":{
		// the name of a device, this must be equal to a device which is detected by lirc. 
		"TV":{
			// defines a font awseome id for the menu icon
			// !\todo we should find another way which does not rely on font awesome.
			"menu_icon_fa":"0x111",
		}, 
		"Another device: {
			// same information like above 
		} // more devcies could follow until eof
	}	
}
