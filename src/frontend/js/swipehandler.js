// swipe listener.
document.addEventListener('touchstart', handleTouchStart, false);
document.addEventListener('touchmove', handleTouchMove, false);
document.addEventListener('touchend', handleTouchEnd, false);

var xDown = null;
var yDown = null;
var xDownLast = null;
var yDownLast = null;

var timeout;
var lastTap = 0;
var hasSwiped = false;
var swipeDirection = ""
var touchCount = 0
var touchTarget = ""

function handleTouchStart(evt) {
    xDown = evt.touches[0].clientX;
    yDown = evt.touches[0].clientY;
    touchTarget = evt.target

    hasSwiped = false

    touchCount++
};


function handleTouchEnd(evt) {
    if (hasSwiped) {
        if ("left" == swipeDirection) {
            switch (touchCount) {
                case 1:
                    handleSwipe(swipeDirection, evt)
                    break
                case 2:
                    handleSwipe("BACK", evt)
                    break
            }
        } else if ("right" == swipeDirection) {
            switch (touchCount) {
                case 1:
                    handleSwipe(swipeDirection, evt)
                    break
                case 2:
                    break
            }
        } else if ("up" == swipeDirection) {
            switch (touchCount) {
                case 1:
                    handleSwipe(swipeDirection, evt)
                    break
                case 2:
                    handleSwipe("VOLUMEUP", evt)
                    break
            }
        } else if ("down" == swipeDirection) {
            switch (touchCount) {
                case 1:
                    handleSwipe(swipeDirection, evt)
                    break
                case 2:
                    handleSwipe("VOLUMEDOWN", evt)
                    break
            }
        } else {
            handleSwipe(swipeDirection, evt)
        }


    } else {
        var currentTime = new Date().getTime();
        var tapLength = currentTime - lastTap;
        clearTimeout(timeout);

        lastTap = currentTime;
        if (tapLength < 500 && tapLength > 0) {
            // Double tap
        } else {
            handleSwipe("ENTER", evt)

            timeout = setTimeout(function() {
                // elm2.innerHTML = 'Single Tap (timeout)';
                clearTimeout(timeout);
            }, 500);
        }
    }

    touchCount = 0
}

function handleTouchMove(evt) {
    if (!xDown || !yDown) {
        return;
    }

    var xUp = evt.touches[0].clientX;
    var yUp = evt.touches[0].clientY;


    var xDiff = xDown - xUp;
    var yDiff = yDown - yUp;
    hasSwiped = true

    if (Math.abs(xDiff) > Math.abs(yDiff)) { /*most significant*/
        if (xDiff > 0) {
            /* left swipe */
            swipeDirection = "left"

        } else if (xDiff < 0) {
            /* right swipe */
            swipeDirection = "right"
        } else {
            hasSwiped = false
        }
    } else {
        if (yDiff > 0) {
            /* up swipe */
            swipeDirection = "up"
        } else if (yDiff < 0) {
            /* down swipe */
            swipeDirection = "down"
        } else {
            hasSwiped = false
        }
    }
};

// register the swipe handler
$("sidebar-wrapper").on("swipe", function() {
    $("#wrapper").toggleClass("toggled");
});


function handleSwipe(direction, evt) {
    var tp = document.getElementById("touchpad");
    var touchpadOpen = tp.style.display == 'block'

    if (_ignore_swipe){
        _ignore_swipe = false
        return    
    }

    if (!touchpadOpen) {
        if (direction == "left") {
            setMenu(true)
        } else if (direction == "right") {
            setMenu(false)
        }
    } else if (touchTarget.id == "touchpadCanvas") {
        evt.preventDefault()
        code = "KEY_" + direction.toUpperCase();


        if (null != _selectedDevice) {
            for (var i = 0; i < _selectedDevice.keys.length; i++) {
                if (_selectedDevice.keys[i].name == code) {
                    handleKey(_selectedDevice.keys[i]);
                }
            }
        }
    }
}