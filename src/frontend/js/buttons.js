

var groupPower = "Main"
var groupVolume = "Audio"
var groupControl = "Control"
var groupColours = "Colours"
var groupPlayback = "Playback"
var groupNumbers = "Numbers"
var groupSource = "Source"
var groupLetters = "Letters"
var groupSpecialChars = "Special Characters"
var groupKeypad = "Keypad"
var groupFunctionKeys = "Function Keys"
var groupBrightness = "Brightness"
var groupDots = "Dots"
var groupModKeys = "Modifiers"
var groupFnKeys = "FN Function"
var groupMail = "Mailing"
var groupProgram = "Program"
var groupApps = "Apps"
var groupFile = "File"
var groupMode = "Mode"
var groupNavigation = "Navigation"
var groupReserved = "-- Reserved for special usage -- "

var groupDefault = getDefaultGroup();
var locationWildCard = "*"

function getEmptyLabel() {
	return String.fromCharCode(0x200C);
}

function getDefaultGroup() {
	return "Miscellaneous";
}

function getNextGroup(currentGroup) {
	switch (currentGroup) {
		case groupPower:
			return { before: locationWildCard, after: null }
		case groupPlayback:
			return { before: groupVolume, after: groupPower }
		case groupVolume:
			return { before: groupControl, after: groupPlayback }
		case groupControl:
			return { before: groupSource, after: groupVolume }
		case groupSource:
			return { before: groupColours, after: groupPlayback }
		case groupColours:
			return { before: groupNumbers, after: groupSource }
		case groupNumbers:
			return { before: groupLetters, after: groupColours }
		case groupLetters:
			return { before: groupSpecialChars, after: groupNumbers }
		case groupSpecialChars:
			return { before: groupKeypad, after: groupLetters }
		case groupKeypad:
			return { before: groupFunctionKeys, after: groupSpecialChars }
		case groupFunctionKeys:
			return { before: groupBrightness, after: groupKeypad }
		case groupBrightness:
			return { before: groupDots, after: groupFunctionKeys }
		case groupDots:
			return { before: groupModKeys, after: groupBrightness }
		case groupModKeys:
			return { before: groupFnKeys, after: groupDots }
		case groupFnKeys:
			return { before: groupMail, after: groupModKeys }
		case groupMail:
			return { before: groupProgram, after: groupFnKeys }
		case groupProgram:
			return { before: groupApps, after: groupMail }
		case groupApps:
			return { before: groupFile, after: groupProgram }
		case groupFile:
			return { before: groupMode, after: groupApps }
		case groupMode:
			return { before: groupNavigation, after: groupFile }
		case groupNavigation:
			return { before: groupDefault, after: groupMode }
		default:
			return { before: null, after: locationWildCard }
	}
}

function getButtonLabels() {
	var buttons = new Array();

	// The first argument will be used for the icon of the button.
	// second argument will be displayed below in a label. 
	// if there is no second; empty char will be display ( meaning 2nd is optional )
	// Third argument, if any will be used as colour

	// This is might not be conslusive

	// see char info http://www.fileformat.info/info/unicode/char/200c/index.htm 
	// ZERO WIDTH NON-JOINER
	var defaultLabel = getEmptyLabel();
	var defaultColour = 0xfffff;
	var defaultFont = "Helvetica, Arial, Sans-Serif";
	var defaultFontSize = -1; // value from css is taken
	var defaultGroup = getDefaultGroup();



	var smallFontSize = 25;
	var verySmallFontSize = 10;
	var tinyFontSize = 8;

	var fontAwesome = "fontAwesome";
	var colourBlue = "blue";
	var colourRed = "red";
	var colourGreen = "green";
	var colourYellow = "yellow";

	var iconMissing = String.fromCharCode(0xf1c5)

	// {button: , label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group:defaultGroup};


	// special buttons
	buttons["KEY_F13"] = { button: String.fromCharCode(0xf008), label: "Bluray", color: defaultColour, font: fontAwesome, fontSize: smallFontSize, group: groupSource };
	buttons["KEY_F14"] = { button: String.fromCharCode(0xf293), label: "Bluetooth", color: defaultColour, font: fontAwesome, fontSize: smallFontSize, group: groupSource };
	buttons["KEY_F15"] = { button: String.fromCharCode(0xf287), label: "Usb", color: defaultColour, font: fontAwesome, fontSize: smallFontSize, group: groupSource };
	buttons["KEY_F16"] = { button: String.fromCharCode(0xf025), label: "Aux1", color: defaultColour, font: fontAwesome, fontSize: smallFontSize, group: groupSource };
	buttons["KEY_F17"] = { button: String.fromCharCode(0xf025), label: "Aux2", color: defaultColour, font: fontAwesome, fontSize: smallFontSize, group: groupSource };

	// reserved to be more special <3
	buttons["KEY_F18"] = { button: String.fromCharCode(0xf0ac), label: "Inet Radio", color: defaultColour, font: fontAwesome, fontSize: smallFontSize, group: groupSource };
	buttons["KEY_F19"] = { button: "F19", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: smallFontSize, group: groupReserved };
	buttons["KEY_F20"] = { button: "F20", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: smallFontSize, group: groupReserved };
	buttons["KEY_F21"] = { button: "F21", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: smallFontSize, group: groupReserved };
	buttons["KEY_F22"] = { button: "F22", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: smallFontSize, group: groupReserved };
	buttons["KEY_F23"] = { button: "F23", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: smallFontSize, group: groupReserved };
	buttons["KEY_F24"] = { button: "F24", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: smallFontSize, group: groupReserved };


	buttons["KEY_102ND"] = { button: "102", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: smallFontSize, group: defaultGroup };
	buttons["KEY_0"] = { button: "0", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupNumbers };
	buttons["KEY_1"] = { button: "1", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupNumbers };
	buttons["KEY_2"] = { button: "2", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupNumbers };
	buttons["KEY_3"] = { button: "3", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupNumbers };
	buttons["KEY_4"] = { button: "4", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupNumbers };
	buttons["KEY_5"] = { button: "5", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupNumbers };
	buttons["KEY_6"] = { button: "6", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupNumbers };
	buttons["KEY_7"] = { button: "7", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupNumbers };
	buttons["KEY_8"] = { button: "8", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupNumbers };
	buttons["KEY_9"] = { button: "9", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupNumbers };
	buttons["KEY_A"] = { button: "A", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupLetters };
	buttons["KEY_AB"] = { button: "AB", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: defaultGroup };
	buttons["KEY_AGAIN"] = { button: String.fromCharCode(0xf01e), label: "Again", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["KEY_ALTERASE"] = { button: String.fromCharCode(0xf12d), label: "Erase", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["KEY_ANGLE"] = { button: String.fromCharCode(0xf209), label: "Angle", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["KEY_APOSTROPHE"] = { button: "'", label: "Apostrophe", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupSpecialChars };
	buttons["KEY_ARCHIVE"] = { button: String.fromCharCode(0xf187), label: "Archive", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupApps };
	buttons["KEY_AUDIO"] = { button: String.fromCharCode(0xf025), label: "Audio", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupSource };
	buttons["KEY_AUX"] = { button: String.fromCharCode(0xf0f1), label: "Aux", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupSource };
	buttons["KEY_B"] = { button: "B", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupLetters };
	buttons["KEY_BACK"] = { button: String.fromCharCode(0xf0a8), label: "Back", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupControl };
	buttons["KEY_BACKSLASH"] = { button: "\\", label: "Backslash", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupSpecialChars };
	buttons["KEY_BACKSPACE"] = { button: String.fromCharCode(0xf0a8), label: "Backspace", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupModKeys };
	buttons["KEY_BASSBOOST"] = { button: String.fromCharCode(0xf012), label: "Bass Boost", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupVolume };
	buttons["KEY_BATTERY"] = { button: String.fromCharCode(0xf241), label: "Battery", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupMode };
	buttons["KEY_BLUE"] = { button: String.fromCharCode(0xf111), label: "Blue", color: colourBlue, font: fontAwesome, fontSize: defaultFontSize, group: groupColours };
	buttons["KEY_BOOKMARKS"] = { button: String.fromCharCode(0xf02e), label: "Bookmarks", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupApps };
	buttons["KEY_BREAK"] = { button: String.fromCharCode(0xf05e), label: "Break", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["KEY_BRIGHTNESSDOWN"] = { button: String.fromCharCode(0xf185) + "-", label: "Bright -", color: defaultColour, font: fontAwesome, fontSize: smallFontSize, group: groupBrightness };
	buttons["KEY_BRIGHTNESSUP"] = { button: String.fromCharCode(0xf185) + "+", label: "Bright +", color: defaultColour, font: fontAwesome, fontSize: smallFontSize, group: groupBrightness };
	buttons["KEY_BRL_DOT1"] = { button: "1", label: "Dot 1", color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupDots };
	buttons["KEY_BRL_DOT2"] = { button: "2", label: "Dot 2", color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupDots };
	buttons["KEY_BRL_DOT3"] = { button: "3", label: "Dot 3", color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupDots };
	buttons["KEY_BRL_DOT4"] = { button: "4", label: "Dot 4", color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupDots };
	buttons["KEY_BRL_DOT5"] = { button: "5", label: "Dot 5", color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupDots };
	buttons["KEY_BRL_DOT6"] = { button: "6", label: "Dot 6", color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupDots };
	buttons["KEY_BRL_DOT7"] = { button: "7", label: "Dot 7", color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupDots };
	buttons["KEY_BRL_DOT8"] = { button: "8", label: "Dot 8", color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupDots };
	buttons["KEY_C"] = { button: "C", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupLetters };
	buttons["KEY_CALC"] = { button: String.fromCharCode(0xf1ec), label: "Calculator", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupApps };
	buttons["KEY_CALENDAR"] = { button: String.fromCharCode(0xf073), label: "Calendar", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupApps };
	buttons["KEY_CAMERA"] = { button: String.fromCharCode(0xf030), label: "Camera", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupApps };
	buttons["KEY_CANCEL"] = { button: String.fromCharCode(0xf05e), label: "Cancel", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["KEY_CAPSLOCK"] = { button: "CAPS", label: "Capslock", color: defaultColour, font: defaultFont, fontSize: tinyFontSize, group: groupModKeys };
	buttons["KEY_CD"] = { button: String.fromCharCode(0xf052), label: "CD", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupSource };
	buttons["KEY_CHANNEL"] = { button: String.fromCharCode(0xf052), label: "Channel", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupControl };
	buttons["KEY_CHANNELDOWN"] = { button: String.fromCharCode(0xf106), label: "Channel", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupControl };
	buttons["KEY_CHANNELUP"] = { button: String.fromCharCode(0xf107), label: "Channel", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupControl };
	buttons["KEY_CHAT"] = { button: String.fromCharCode(0xf086), label: "Chat", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupApps };
	buttons["KEY_CLEAR"] = { button: String.fromCharCode(0xf1f8), label: "Clear", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupNavigation };
	buttons["KEY_CLOSE"] = { button: String.fromCharCode(0xf00d), label: "Close", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupFile };
	buttons["KEY_CLOSECD"] = { button: String.fromCharCode(0xf052), label: "Close CD", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupPower };
	buttons["KEY_COFFEE"] = { button: String.fromCharCode(0xf0f4), label: "Coffee", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupApps };
	buttons["KEY_COMMA"] = { button: ",", label: "Comma", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupSpecialChars };
	buttons["KEY_COMPOSE"] = { button: String.fromCharCode(0xf044), label: "Compose", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupMail };
	buttons["KEY_COMPUTER"] = { button: String.fromCharCode(0xf109), label: "Computer", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupSource };
	buttons["KEY_CONFIG"] = { button: String.fromCharCode(0xf085), label: "Config", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupControl };
	buttons["KEY_CONNECT"] = { button: String.fromCharCode(0xf1e6), label: "Connect", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupPower };
	buttons["KEY_COPY"] = { button: String.fromCharCode(0xf0ea), label: "Copy", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupFile };
	buttons["KEY_CUT"] = { button: String.fromCharCode(0xf0c4), label: "Cut", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupFile };
	buttons["KEY_CYCLEWINDOWS"] = { button: String.fromCharCode(0xf0ae), label: "Cycle Win", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["KEY_D"] = { button: "D", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupLetters };
	buttons["KEY_DEL_EOL"] = { button: String.fromCharCode(0xf014), label: "Delete EOL", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["KEY_DEL_EOS"] = { button: String.fromCharCode(0xf014), label: "Delete EOS", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["KEY_DELETE"] = { button: String.fromCharCode(0xf014), label: "Delete", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupFile };
	buttons["KEY_DELETEFILE"] = { button: String.fromCharCode(0xf014), label: "Delete File", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupFile };
	buttons["KEY_DEL_LINE"] = { button: String.fromCharCode(0xf014), label: "Delete Line", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupFile };
	buttons["KEY_DIGITS"] = { button: "#", label: "Digits", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["KEY_DIRECTION"] = { button: String.fromCharCode(0xf0b2), label: "Direction", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupNavigation };
	buttons["KEY_DIRECTORY"] = { button: String.fromCharCode(0xf07b), label: "Directory", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupFile };
	buttons["KEY_DOCUMENTS"] = { button: String.fromCharCode(0xf15c), label: "Documents", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupFile };
	buttons["KEY_DOT"] = { button: ".", label: "Dot", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupSpecialChars };
	buttons["KEY_DOWN"] = { button: String.fromCharCode(0xf063), label: "Down", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupControl };
	buttons["KEY_DVD"] = { button: String.fromCharCode(0xf111), label: "DVD", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupSource };
	buttons["KEY_E"] = { button: "E", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupLetters };
	buttons["KEY_EDIT"] = { button: String.fromCharCode(0xf044), label: "Edit", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupMail };
	buttons["KEY_EJECTCD"] = { button: String.fromCharCode(0xf052), label: "Eject CD", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupPower };
	buttons["KEY_EJECTCLOSECD"] = { button: String.fromCharCode(0xf052), label: "Eject, Close", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupPower };
	buttons["KEY_EMAIL"] = { button: String.fromCharCode(0xf0e0), label: "E-Mail", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupMail };
	buttons["KEY_END"] = { button: String.fromCharCode(0xf256), label: "End", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupNavigation };
	buttons["KEY_ENTER"] = { button: String.fromCharCode(0xf090), label: "Enter", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupControl };
	buttons["KEY_EPG"] = { button: String.fromCharCode(0xf05a), label: "EPG", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupControl };
	buttons["KEY_EQUAL"] = { button: "=", label: "Equal", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupSpecialChars };
	buttons["KEY_ESC"] = { button: String.fromCharCode(0xf00d), label: "Esc", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupModKeys };
	buttons["KEY_EXIT"] = { button: String.fromCharCode(0xf00d), label: "Exit", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupNavigation };
	buttons["KEY_F"] = { button: "F", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: smallFontSize, group: groupLetters };
	buttons["KEY_F1"] = { button: "F1", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: smallFontSize, group: groupFunctionKeys };
	buttons["KEY_F2"] = { button: "F2", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: smallFontSize, group: groupFunctionKeys };
	buttons["KEY_F3"] = { button: "F3", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: smallFontSize, group: groupFunctionKeys };
	buttons["KEY_F4"] = { button: "F4", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: smallFontSize, group: groupFunctionKeys };
	buttons["KEY_F5"] = { button: "F5", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: smallFontSize, group: groupFunctionKeys };
	buttons["KEY_F6"] = { button: "F6", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: smallFontSize, group: groupFunctionKeys };
	buttons["KEY_F7"] = { button: "F7", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: smallFontSize, group: groupFunctionKeys };
	buttons["KEY_F8"] = { button: "F8", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: smallFontSize, group: groupFunctionKeys };
	buttons["KEY_F9"] = { button: "F9", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: smallFontSize, group: groupFunctionKeys };
	buttons["KEY_F10"] = { button: "F10", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: smallFontSize, group: groupFunctionKeys };
	buttons["KEY_F11"] = { button: "F11", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: smallFontSize, group: groupFunctionKeys };
	buttons["KEY_F12"] = { button: "F12", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: smallFontSize, group: groupFunctionKeys };
	buttons["KEY_FASTFORWARD"] = { button: String.fromCharCode(0xf050), label: "Fast Forward", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupPlayback };
	buttons["KEY_FAVORITES"] = { button: String.fromCharCode(0xf005), label: "Favorite", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupSource };
	buttons["KEY_FILE"] = { button: String.fromCharCode(0xf052), label: "File", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupSource };
	buttons["KEY_FINANCE"] = { button: String.fromCharCode(0xf0d6), label: "Finance", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupApps };
	buttons["KEY_FIND"] = { button: String.fromCharCode(0xf002), label: "Find", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupFile };
	buttons["KEY_FIRST"] = { button: "1st", label: "Fisrt", color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupNavigation };
	buttons["KEY_FN"] = { button: "Fn", label: "Fn", color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupFnKeys };
	buttons["KEY_FN_2"] = { button: "Fn2", label: "Fn2", color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupFnKeys };
	buttons["KEY_FN_1"] = { button: "Fn1", label: "Fn1", color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupFnKeys };
	buttons["KEY_FN_B"] = { button: "-", label: "Fn B", color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupFnKeys };
	buttons["KEY_FN_D"] = { button: "-", label: "Fn D", color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupFnKeys };
	buttons["KEY_FN_E"] = { button: "-", label: "Fn E", color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupFnKeys };
	buttons["KEY_FN_ESC"] = { button: "-", label: "Fn ESC", color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupFnKeys };
	buttons["KEY_FN_F"] = { button: "-", label: "Fn F", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupFnKeys };
	buttons["KEY_FN_F10"] = { button: "-", label: "Fn F10", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupFnKeys };
	buttons["KEY_FN_F1"] = { button: "-", label: "Fn F1", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupFnKeys };
	buttons["KEY_FN_F11"] = { button: "-", label: "Fn F11", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupFnKeys };
	buttons["KEY_FN_F12"] = { button: "-", label: "Fn F12", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupFnKeys };
	buttons["KEY_FN_F2"] = { button: "-", label: "Fn F2", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupFnKeys };
	buttons["KEY_FN_F3"] = { button: "-", label: "Fn F3", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupFnKeys };
	buttons["KEY_FN_F4"] = { button: "-", label: "Fn F4", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupFnKeys };
	buttons["KEY_FN_F5"] = { button: "-", label: "Fn F5", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupFnKeys };
	buttons["KEY_FN_F6"] = { button: "-", label: "Fn F6", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupFnKeys };
	buttons["KEY_FN_F7"] = { button: "-", label: "Fn F7", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupFnKeys };
	buttons["KEY_FN_F8"] = { button: "-", label: "Fn F8", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupFnKeys };
	buttons["KEY_FN_F9"] = { button: "-", label: "Fn F9", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupFnKeys };
	buttons["KEY_FN_S"] = { button: "-", label: "Fn S", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupFnKeys };
	buttons["KEY_FORWARD"] = { button: String.fromCharCode(0xf04e), label: "Forward", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupPlayback };
	buttons["KEY_FORWARDMAIL"] = { button: String.fromCharCode(0xf064), label: "Forward Mail", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupMail };
	buttons["KEY_FRONT"] = { button: String.fromCharCode(0xf04e), label: "Front", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupPlayback };
	buttons["KEY_G"] = { button: "G", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupLetters };
	buttons["KEY_GAME"] = { button: String.fromCharCode(0xf11b), label: "Game", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupSource };
	buttons["KEY_GOTO"] = { button: String.fromCharCode(0xf21c), label: "Goto", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupSource };
	buttons["KEY_GRAVE"] = { button: "`", label: "Accent Grave", color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupSpecialChars };
	buttons["KEY_GREEN"] = { button: String.fromCharCode(0xf111), label: "Green", color: colourGreen, font: fontAwesome, fontSize: defaultFontSize, group: groupColours };
	buttons["KEY_H"] = { button: "H", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupLetters };
	buttons["KEY_HANGEUL"] = { button: iconMissing, label: "HANGEUL", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["KEY_HANJA"] = { button: iconMissing, label: "Hanja", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["KEY_HELP"] = { button: String.fromCharCode(0xf059), label: "Help", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupPower };
	buttons["KEY_HENKAN"] = { button: iconMissing, label: "Henkan", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["KEY_HIRAGANA"] = { button: iconMissing, label: "Hirgana", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["KEY_HOME"] = { button: String.fromCharCode(0xf015), label: "Home", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupPower };
	buttons["KEY_HOMEPAGE"] = { button: String.fromCharCode(0xf015), label: "Homepage", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupApps };
	buttons["KEY_HP"] = { button: "HP", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: defaultGroup };
	buttons["KEY_I"] = { button: "I", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupLetters };
	buttons["KEY_INFO"] = { button: String.fromCharCode(0xf05a), label: "Info", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupPower };
	buttons["KEY_INSERT"] = { button: String.fromCharCode(0xf0ea), label: "Insert", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupFile };
	buttons["KEY_INS_LINE"] = { button: String.fromCharCode(0xf0ea), label: "Insert Line", color: fontAwesome, font: fontAwesome, fontSize: defaultFontSize, group: groupFile };
	buttons["KEY_ISO"] = { button: iconMissing, label: "Iso", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["KEY_J"] = { button: "J", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupLetters };
	buttons["KEY_K"] = { button: "K", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupLetters };
	buttons["KEY_KEYBOARD"] = { button: String.fromCharCode(0xf11c), label: "Keyboard", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupSource };
	buttons["KEY_KP0"] = { button: "0", label: "Keypad 0", color: defaultColour, font: defaultFont, fontSize: smallFontSize, group: groupKeypad };
	buttons["KEY_KP1"] = { button: "1", label: "Keypad 1", color: defaultColour, font: defaultFont, fontSize: smallFontSize, group: groupKeypad };
	buttons["KEY_KP2"] = { button: "2", label: "Keypad 2", color: defaultColour, font: defaultFont, fontSize: smallFontSize, group: groupKeypad };
	buttons["KEY_KP3"] = { button: "3", label: "Keypad 3", color: defaultColour, font: defaultFont, fontSize: smallFontSize, group: groupKeypad };
	buttons["KEY_KP4"] = { button: "4", label: "Keypad 4", color: defaultColour, font: defaultFont, fontSize: smallFontSize, group: groupKeypad };
	buttons["KEY_KP5"] = { button: "5", label: "Keypad 5", color: defaultColour, font: defaultFont, fontSize: smallFontSize, group: groupKeypad };
	buttons["KEY_KP6"] = { button: "6", label: "Keypad 6", color: defaultColour, font: defaultFont, fontSize: smallFontSize, group: groupKeypad };
	buttons["KEY_KP7"] = { button: "7", label: "Keypad 7", color: defaultColour, font: defaultFont, fontSize: smallFontSize, group: groupKeypad };
	buttons["KEY_KP8"] = { button: "8", label: "Keypad 8", color: defaultColour, font: defaultFont, fontSize: smallFontSize, group: groupKeypad };
	buttons["KEY_KP9"] = { button: "9", label: "Keypad 9", color: defaultColour, font: defaultFont, fontSize: smallFontSize, group: groupKeypad };
	buttons["KEY_KPASTERISK"] = { button: "*", label: "Asteriks", color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupKeypad };
	buttons["KEY_KPCOMMA"] = { button: ",", label: "KP Comma", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupKeypad };
	buttons["KEY_KPDOT"] = { button: ".", label: "KP Dot", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupKeypad };
	buttons["KEY_KPENTER"] = { button: String.fromCharCode(0xf090), label: "KP ENTER", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupKeypad };
	buttons["KEY_KPEQUAL"] = { button: "=", label: "KP EQUAL", color: defaultColour, font: defaultFontSize, fontSize: defaultFontSize, group: groupKeypad };
	buttons["KEY_KPJPCOMMA"] = { button: ",", label: "Kpjp Comma", color: defaultColour, font: defaultFontSize, fontSize: defaultFontSize, group: groupKeypad };
	buttons["KEY_KPLEFTPAREN"] = { button: String.fromCharCode(0xf060), label: "KpLeft", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupKeypad };
	buttons["KEY_KPMINUS"] = { button: "-", label: "KP Minus", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupKeypad };
	buttons["KEY_KPPLUS"] = { button: "+", label: "KP Plus", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupKeypad };
	buttons["KEY_KPPLUSMINUS"] = { button: "+-", label: "Plus Minus", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupKeypad };
	buttons["KEY_KPRIGHTPAREN"] = { button: String.fromCharCode(0xf061), label: "KP Right", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupKeypad };
	buttons["KEY_KPSLASH"] = { button: "/", label: "KP Slash", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupKeypad };
	buttons["KEY_L"] = { button: "L", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupLetters };
	buttons["KEY_LANGUAGE"] = { button: String.fromCharCode(0xf1ab), label: "Language", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupPower };
	buttons["KEY_LAST"] = { button: "-", label: "Last", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupNavigation };
	buttons["KEY_LEFT"] = { button: String.fromCharCode(0xf060), label: "Left", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupControl };
	buttons["KEY_LEFTALT"] = { button: "ALT", label: "Left Alt", color: defaultColour, font: defaultFont, fontSize: tinyFontSize, group: groupModKeys };
	buttons["KEY_LEFTBRACE"] = { button: "{", label: "Left Brace", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupSpecialChars };
	buttons["KEY_LEFTCTRL"] = { button: "CTRL", label: "Left Ctrl", color: defaultColour, font: defaultFont, fontSize: tinyFontSize, group: groupModKeys };
	buttons["KEY_LEFTMETA"] = { button: String.fromCharCode(0xf17a), label: "Left Meta", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupModKeys };
	buttons["KEY_LEFTSHIFT"] = { button: "SHIFT", label: "Left Shift", color: defaultColour, font: defaultFont, fontSize: tinyFontSize, group: groupModKeys };
	buttons["KEY_LINEFEED"] = { button: "LF", label: "Linefeed", color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: defaultGroup };
	buttons["KEY_LIST"] = { button: String.fromCharCode(0xf03a), label: "List", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupNavigation };
	buttons["KEY_M"] = { button: "M", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupLetters };
	buttons["KEY_MACRO"] = { button: String.fromCharCode(0xf1c9), label: "Macro", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupProgram };
	buttons["KEY_MAIL"] = { button: String.fromCharCode(0xf0e0), label: "Mail", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupMail };
	buttons["KEY_MAX"] = { button: "^", label: "Max", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupNavigation };
	buttons["KEY_MEDIA"] = { button: String.fromCharCode(0xf108), label: "Media", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupSource };
	buttons["KEY_MEMO"] = { button: String.fromCharCode(0xf249), label: "Memo", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupMail };
	buttons["KEY_MENU"] = { button: String.fromCharCode(0xf0c9), label: "Menu", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupPower };
	buttons["KEY_MHP"] = { button: iconMissing, label: "MHP", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["KEY_MINUS"] = { button: "-", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupSpecialChars };
	buttons["KEY_MODE"] = { button: String.fromCharCode(0xf07e), label: "Mode", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupMode };
	buttons["KEY_MOVE"] = { button: String.fromCharCode(0xf047), label: "Move", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupNavigation };
	buttons["KEY_MP3"] = { button: String.fromCharCode(0xf001), label: "MP3", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupSource };
	buttons["KEY_MSDOS"] = { button: String.fromCharCode(0xf120), label: "MS-DOS", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupSource };
	buttons["KEY_MUHENKAN"] = { button: iconMissing, label: "Muhekan", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["KEY_MUTE"] = { button: String.fromCharCode(0xf026), label: "Mute", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupVolume };
	buttons["KEY_N"] = { button: "N", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupLetters };
	buttons["KEY_NEW"] = { button: String.fromCharCode(0xf15b), label: "New", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupFile };
	buttons["KEY_NEXT"] = { button: String.fromCharCode(0xf051), label: "Next", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupPlayback };
	buttons["KEY_NEXTSONG"] = { button: String.fromCharCode(0xf051), label: "Next Song", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupPlayback };
	buttons["KEY_NUMLOCK"] = { button: "Num", label: "Numlock", color: defaultColour, font: defaultFont, fontSize: tinyFontSize, group: groupModKeys };
	buttons["KEY_O"] = { button: "Q", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupLetters };
	buttons["KEY_OK"] = { button: String.fromCharCode(0xf058), label: "OK", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupControl };
	buttons["KEY_OPEN"] = { button: String.fromCharCode(0xf07c), label: "Open", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupFile };
	buttons["KEY_OPTION"] = { button: String.fromCharCode(0xf013), label: "Option", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupPower };
	buttons["KEY_P"] = { button: "P", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupLetters };
	buttons["KEY_PAGEDOWN"] = { button: String.fromCharCode(0xf149), label: "Pagedown", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupNavigation };
	buttons["KEY_PAGEUP"] = { button: String.fromCharCode(0xf148), label: "Pageup", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupNavigation };
	buttons["KEY_PASTE"] = { button: String.fromCharCode(0xf0ea), label: "Paste", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupFile };
	buttons["KEY_PAUSE"] = { button: String.fromCharCode(0xf04c), label: "Pause", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupPlayback };
	buttons["KEY_PAUSECD"] = { button: String.fromCharCode(0xf04c), label: "Pause CD", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupPlayback };
	buttons["KEY_PC"] = { button: String.fromCharCode(0xf108), label: "PC", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupSource };
	buttons["KEY_PHONE"] = { button: String.fromCharCode(0xf095), label: "Phone", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupSource };
	buttons["KEY_PLAY"] = { button: String.fromCharCode(0xf04b), label: "Play", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupPlayback };
	buttons["KEY_PLAYCD"] = { button: String.fromCharCode(0xf04b), label: "Play CD", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupPlayback };
	buttons["KEY_PLAYER"] = { button: String.fromCharCode(0xf16a), label: "Player", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupPlayback };
	buttons["KEY_PLAYPAUSE"] = { button: String.fromCharCode(0xf04b) + String.fromCharCode(0xf04c), label: "Play / Pause", color: defaultColour, font: fontAwesome, fontSize: smallFontSize, group: groupPlayback };
	buttons["KEY_POWER"] = { button: String.fromCharCode(0xf011), label: "Power", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupPower };
	buttons["KEY_POWER2"] = { button: String.fromCharCode(0xf011), label: "Power 2", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupPower };
	buttons["KEY_PREVIOUS"] = { button: String.fromCharCode(0xf048), label: "Previous", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupPlayback };
	buttons["KEY_PREVIOUSSONG"] = { button: String.fromCharCode(0xf048), label: "Prev. Song", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupPlayback };
	buttons["KEY_PRINT"] = { button: String.fromCharCode(0xf02f), label: "Print", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupMail };
	buttons["KEY_PROG1"] = { button: String.fromCharCode(0xf0ae), label: "Prog 1", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupProgram };
	buttons["KEY_PROG2"] = { button: String.fromCharCode(0xf0ae), label: "Prog 2", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupProgram };
	buttons["KEY_PROG3"] = { button: String.fromCharCode(0xf0ae), label: "Prog 3", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupProgram };
	buttons["KEY_PROG4"] = { button: String.fromCharCode(0xf0ae), label: "Prog 4", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupProgram };
	buttons["KEY_PROGRAM"] = { button: String.fromCharCode(0xf0ae), label: "Program", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupProgram };
	buttons["KEY_PROPS"] = { button: String.fromCharCode(0xf0ad), label: "Propperties", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupPower };
	buttons["KEY_PVR"] = { button: String.fromCharCode(0xf03d), label: "PVR", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupSource };
	buttons["KEY_Q"] = { button: "Q", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupLetters };
	buttons["KEY_QUESTION"] = { button: String.fromCharCode(0xf059), label: "Question", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupPower };
	buttons["KEY_R"] = { button: "R", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupLetters };
	buttons["KEY_RADIO"] = { button: String.fromCharCode(0xf025), label: "Radio", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupSource };
	buttons["KEY_RECORD"] = { button: String.fromCharCode(0xf083), label: "Record", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupApps };
	buttons["KEY_RED"] = { button: String.fromCharCode(0xf111), label: "Red", color: colourRed, font: fontAwesome, fontSize: defaultFontSize, group: groupColours };
	buttons["KEY_REDO"] = { button: String.fromCharCode(0xf01e), label: "Redo", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupFile };
	buttons["KEY_REFRESH"] = { button: String.fromCharCode(0xf021), label: "Refresh", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupMail };
	buttons["KEY_REPLY"] = { button: String.fromCharCode(0xf112), label: "Reply", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupMail };
	buttons["KEY_RESERVED"] = { button: String.fromCharCode(0xf0c7), label: "Reserved", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["KEY_RESTART"] = { button: String.fromCharCode(0xf01e), label: "Restart", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupPower };
	buttons["KEY_REWIND"] = { button: String.fromCharCode(0xf04a), label: "Rewind", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupPlayback };
	buttons["KEY_RIGHT"] = { button: String.fromCharCode(0xf061), label: "Right", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupControl };
	buttons["KEY_RIGHTALT"] = { button: "ALT", label: "Right Alt", color: defaultColour, font: defaultFont, fontSize: tinyFontSize, group: groupModKeys };
	buttons["KEY_RIGHTBRACE"] = { button: "}", label: "Right Brace", color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupSpecialChars };
	buttons["KEY_RIGHTCTRL"] = { button: "CTRL", label: "Right Ctrl", color: defaultColour, font: defaultFont, fontSize: tinyFontSize, group: groupModKeys };
	buttons["KEY_RIGHTMETA"] = { button: String.fromCharCode(0xf17a), label: "Right Meta", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupModKeys };
	buttons["KEY_RIGHTSHIFT"] = { button: "SHIFT", label: "Right Shift", color: defaultColour, font: defaultFont, fontSize: tinyFontSize, group: groupModKeys };
	buttons["KEY_RO"] = { button: String.fromCharCode(0xf023), label: "Read Only", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupFile };
	buttons["KEY_S"] = { button: "S", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupLetters };
	buttons["KEY_SAT"] = { button: String.fromCharCode(0xf197), label: "Sat 1", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupSource };
	buttons["KEY_SAT2"] = { button: String.fromCharCode(0xf197), label: "Sat 2", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupSource };
	buttons["KEY_SAVE"] = { button: String.fromCharCode(0xf0c7), label: "Save", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupFile };
	buttons["KEY_SCREEN"] = { button: String.fromCharCode(0xf108), label: "Screen", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupSource };
	buttons["KEY_SCROLLDOWN"] = { button: String.fromCharCode(0xf0d7), label: "Scroll Down", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupNavigation };
	buttons["KEY_SCROLLLOCK"] = { button: String.fromCharCode(0xf023), label: "Scroll Lock", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupModKeys };
	buttons["KEY_SCROLLUP"] = { button: String.fromCharCode(0xf0d8), label: "Scroll Up", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupNavigation };
	buttons["KEY_SEARCH"] = { button: String.fromCharCode(0xf002), label: "Search", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupFile };
	buttons["KEY_SELECT"] = { button: String.fromCharCode(0xf1de), label: "Select", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupNavigation };
	buttons["KEY_SEMICOLON"] = { button: ";", label: ";", color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupSpecialChars };
	buttons["KEY_SEND"] = { button: String.fromCharCode(0xf1d8), label: "Send", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupMail };
	buttons["KEY_SENDFILE"] = { button: String.fromCharCode(0xf0c5), label: "Send File", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupFile };
	buttons["KEY_SETUP"] = { button: String.fromCharCode(0xf251), label: "Setup", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupPower };
	buttons["KEY_SHOP"] = { button: String.fromCharCode(0xf07a), label: "Shop", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupNavigation };
	buttons["KEY_SHUFFLE"] = { button: String.fromCharCode(0xf074), label: "Shuffle", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupPlayback };
	buttons["KEY_SLASH"] = { button: "/", label: "Slash", color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupSpecialChars };
	buttons["KEY_SLEEP"] = { button: String.fromCharCode(0xf236), label: "Sleep", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupMode };
	buttons["KEY_SLOW"] = { button: String.fromCharCode(0xf1b9), label: "Slow", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["KEY_SOUND"] = { button: String.fromCharCode(0xf001), label: "Sound", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupVolume };
	buttons["KEY_SPACE"] = { button: String.fromCharCode(0xf065), label: "Space", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupModKeys };
	buttons["KEY_SPORT"] = { button: String.fromCharCode(0xf1e3), label: "Sport", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupApps };
	buttons["KEY_STOP"] = { button: String.fromCharCode(0xf04d), label: "Stop", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupPlayback };
	buttons["KEY_STOPCD"] = { button: String.fromCharCode(0xf04d), label: "Stop CD", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupPlayback };
	buttons["KEY_SUBTITLE"] = { button: String.fromCharCode(0xf086), label: "Subtitle", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupVolume };
	buttons["KEY_SUSPEND"] = { button: String.fromCharCode(0xf236), label: "Suspend", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupMode };
	buttons["KEY_SWITCHVIDEOMODE"] = { button: String.fromCharCode(0xf0ec), label: "Switch Mode", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupMode };
	buttons["KEY_SYSRQ"] = { button: String.fromCharCode(0xf120), label: "SysRq", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["KEY_T"] = { button: "T", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupLetters };
	buttons["KEY_TAB"] = { button: String.fromCharCode(0xf0ec), label: "Tab", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupModKeys };
	buttons["KEY_TAPE"] = { button: String.fromCharCode(0xf1c0), label: "Tape", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupSource };
	buttons["KEY_TEEN"] = { button: String.fromCharCode(0xf1ae), label: "Teen", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["KEY_TEXT"] = { button: String.fromCharCode(0xf031), label: "Text", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["KEY_TIME"] = { button: String.fromCharCode(0xf017), label: "Time", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["KEY_TITLE"] = { button: String.fromCharCode(0xf1dc), label: "Title", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["KEY_TUNER"] = { button: String.fromCharCode(0xf26c), label: "Tuner", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupSource };
	buttons["KEY_TV"] = { button: String.fromCharCode(0xf26c), label: "TV", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupSource };
	buttons["KEY_TV2"] = { button: String.fromCharCode(0xf26c), label: "TV 2", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupSource };
	buttons["KEY_TWEN"] = { button: String.fromCharCode(0xf183), label: "Twen", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup }; // like teen but only in 20s ?!
	buttons["KEY_U"] = { button: "U", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupLetters };
	buttons["KEY_UNDO"] = { button: String.fromCharCode(0xf01e), label: "Undo", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupFile };
	buttons["KEY_UNKNOWN"] = { button: String.fromCharCode(0xf059), label: "Unknown", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["KEY_UP"] = { button: String.fromCharCode(0xf062), label: "Up", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupControl };
	buttons["KEY_V"] = { button: "V", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupLetters };
	buttons["KEY_VCR"] = { button: String.fromCharCode(0xf03d), label: "VCR", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupSource };
	buttons["KEY_VCR2"] = { button: String.fromCharCode(0xf03d), label: "VCR 2", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupSource };
	buttons["KEY_VENDOR"] = { button: String.fromCharCode(0xf1f9), label: "Vendor", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["KEY_VIDEO"] = { button: String.fromCharCode(0xf1c8), label: "Video", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupSource };
	buttons["KEY_VOLUMEDOWN"] = { button: String.fromCharCode(0xf027), label: "Volume Down", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupVolume };
	buttons["KEY_VOLUMEUP"] = { button: String.fromCharCode(0xf028), label: "Volume Up", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupVolume };
	buttons["KEY_W"] = { button: "W", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupLetters };
	buttons["KEY_WAKEUP"] = { button: String.fromCharCode(0xf0f3), label: "Wakeup", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupMode };
	buttons["KEY_WWW"] = { button: String.fromCharCode(0xf0ac), label: "www", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupSource };
	buttons["KEY_X"] = { button: "X", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupLetters };
	buttons["KEY_XFER"] = { button: iconMissing, label: "Xfer", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["KEY_Y"] = { button: "Y", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupLetters };
	buttons["KEY_YELLOW"] = { button: String.fromCharCode(0xf111), label: "Yellow", color: colourYellow, font: fontAwesome, fontSize: defaultFontSize, group: groupColours };
	buttons["KEY_YEN"] = { button: String.fromCharCode(0xf157), label: "Yen", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["KEY_Z"] = { button: "Z", label: defaultLabel, color: defaultColour, font: defaultFont, fontSize: defaultFontSize, group: groupLetters };
	buttons["KEY_ZOOM"] = { button: String.fromCharCode(0xf00e), label: "Zoom", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupNavigation };
	buttons["BTN_0"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_1"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_2"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_3"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_4"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_5"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_6"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_7"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_8"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_9"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_A"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_B"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_BACK"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_BASE"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_BASE2"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_BASE3"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_BASE4"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_BASE5"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_BASE6"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_C"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_DEAD"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_DIGI"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_EXTRA"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_FORWARD"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: groupPlayback };
	buttons["BTN_GAMEPAD"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_GEAR_DOWN"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_GEAR_UP"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_JOYSTICK"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_LEFT"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_MIDDLE"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_MISC"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_MODE"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_MOUSE"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_PINKIE"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_RIGHT"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_SELECT"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_SIDE"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_START"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_STYLUS"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_STYLUS2"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_TASK"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_THUMB"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_THUMB2"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_THUMBL"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_THUMBR"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_TL"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_TL2"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_TOOL_AIRBRUSH"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_TOOL_BRUSH"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_TOOL_DOUBLETAP"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_TOOL_FINGER"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_TOOL_LENS"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_TOOL_MOUSE"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_TOOL_PEN"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_TOOL_PENCIL"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_TOOL_RUBBER"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_TOOL_TRIPLETAP"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_TOP"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_TOP2"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_TOUCH"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_TR"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_TR2"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_TRIGGER"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_WHEEL"] = { button: iconMissing, label: "Color Wheel", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_X"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_Y"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };
	buttons["BTN_Z"] = { button: iconMissing, label: defaultLabel, color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };

	// NOT LIRC KEYS:	
	buttons["STAT_TEMPERATURE"] = { button: String.fromCharCode(0xf2c9), label: "Temperature", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };

	buttons["default"] = { button: iconMissing, label: "N.A.", color: defaultColour, font: fontAwesome, fontSize: defaultFontSize, group: defaultGroup };

	return buttons;
}
