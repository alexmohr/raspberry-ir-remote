//
// Fields
// 

var _logVisible = false;
var _logContainerName = 'log';
var _cookieSelectedDevice = "selectedDevice"

var _selectedDevice = null;

var _mainButtonContainerName = "buttons"
var _mainButtonContainer;

// The last selected menu entry
var _lastListItem = null;

var _lastListItemStyle = null;

// the device selection menu.
var _deviceSelect = "nav"

// All available devices.
var _devices = {};

// the device selection
var _menuNav;

/**
 * All available jobs
 *
 * @type       {Dictonary}
 */
var _jobs = {}

var _menuVisible = false;

var _touchpadCanvas;

var _deviceLoaded = false;
var _jobsLoaded = false;

var _ignore_swipe = false;


var _value_callbacks = {}

// register load function
window.onload = windowOnLoad;

//
// Window loaded function
//
function windowOnLoad() {
    setLog(false)

    openSocket(socketDataReceived)

    var tp = document.getElementById("showTouchPad")
    if (!window.mobileAndTabletcheck())
        tp.style.display = "none"

    _mainButtonContainer = document.getElementById(_mainButtonContainerName);
    _touchpadCanvas = document.getElementById("touchpadCanvas")
    _menuNav = document.getElementById(_deviceSelect);

    // register for hashchanged
    if ("onhashchange" in window) { // event supported?
        window.onhashchange = function () {
            hashChanged(window.location.hash);
        }
    } else { // event not supported:
        var storedHash = window.location.hash;
        window.setInterval(function () {
            if (window.location.hash != storedHash) {
                storedHash = window.location.hash;
                hashChanged(storedHash);
            }
        }, 100);
    }

    initDevices();
    initJobs();

    // register the on changed event for the device selection
    var devSelect = document.getElementById(_deviceSelect);
    devSelect.onchange = selectionChanged;

    var lastLocation = getCookie(_cookieSelectedDevice)
    var deviceSelection = document.getElementById(_deviceSelect);

    deviceSelection.value = lastLocation;
    window.location.href = '#' + lastLocation;
}



function hashChanged(hash) {
    selectionChanged();
}


function selectionChanged() {
    var hash = window.location.hash.substring(1);
    if (hash.length < 1 || hash == "undefined")
        return;

    var header = document.getElementById("header");

    var updateHeader = false;
    var closeMenu = false;
    var closeLog = false;
    var foundDevice = false;
    var showTouchpanel = false;
    var saveLocation = true;

    if (hash == "showLog") {
        removeAllButtons();
        setLog(true)

        header.innerHTML = "Log";

        showTouchpanel = false
        closeMenu = true;
    } else if (hash == "clearLog") {
        var logtext = document.getElementById('logText');
        logtext.innerHTML = "";
        saveLocation = false

        closeMenu = true;
    } else if (hash == "toggleMenu") {
        setMenu(_menuVisible);
        window.location.href = "#"
    } else if (hash == "reloadVidarCfg") {
        saveLocation = false
        reloadVidarConfig()
    } else if (hash == "showTouchpad") {
        saveLocation = false
    } else {
        // try to select a device. 
        for (var key in _devices) {
            if (key == hash) {
                removeAllButtons();
                if (hash == "Jobs") {
                    addJobs(_devices[key])
                }

                deviceSelectionChanged(_devices[key]);

                updateHeader = true;
                closeLog = true;
                closeMenu = true;
                foundDevice = true;
                showTouchpanel = true;

                break;
            }
        }
    }

    if (closeMenu) {
        setMenu(closeMenu);
    }

    if (showTouchpanel) {
        var isMobile = window.mobileAndTabletcheck();
    }

    if (updateHeader) {
        header.innerHTML = hash;
    }

    if (closeLog) {
        setLog(false)
    }

    if (null != _lastListItem) {
        _lastListItem.style = _lastListItemStyle;
    }

    var newListItem = document.getElementById(hash);
    if (null != newListItem) {
        _lastListItem = newListItem
        _lastListItemStyle = _lastListItem.style;
    }


    // this is the case if an item without menu entry is selected
    if (null != _lastListItem) {
        _lastListItem.style.color = "#EEEEEE";
        //_lastListItem.style.backgroundColor = "#E65100";
        _lastListItem.style.backgroundColor = "#0d47a1";
    }

    if (saveLocation) {
        setCookie(_cookieSelectedDevice, hash, 9999 /* life very long*/);
        document.cookie = _cookieSelectedDevice + hash;
    }

}

function setMenu(close) {
    if (close && _menuVisible) {
        _menuVisible = false;
        $("#wrapper").toggleClass("toggled", null, false);
    } else if (!close && !_menuVisible) {
        $("#wrapper").toggleClass("toggled", null, true);
        _menuVisible = true;
    }

    window.location.href = '#';
}

function deviceSelectionChanged(dev) {
    // the combobox containg the device selection
    _selectedDevice = dev;

    enableSupportedKeysForDevice(dev);
}

function setLog(visible) {
    var logDiv = document.getElementById(_logContainerName);
    var clearLog = document.getElementById("btn-clearLog");

    if (!visible) {
        logDiv.style.display = 'none';
        clearLog.style.display = 'none';
    } else {
        logDiv.style.display = 'block';
        clearLog.style.display = '';
    }
}

function toggleLog() {
    _logVisible = !_logVisible;
    setLog(_logVisible)
}

//
// Remove all buttons from the DOM
//
function removeAllButtons() {
    while (_mainButtonContainer.hasChildNodes()) {
        _mainButtonContainer.removeChild(_mainButtonContainer.lastChild);
    }
}

function enableSupportedKeysForDevice(device) {

    // this handles keys. 
    for (var key in device.keys) {
        var key = device.keys[key];
        addButton(device, key);
    }
}

function createJob(job, jobs) {
    var entry = document.createElement("div")
    _mainButtonContainer.appendChild(entry);
    entry.className = "jobEntry"

    var leftItemName = "jobNameLeft_" + jobs[job]["name"];
    var rightItemName = "jobNameRight_" + jobs[job]["name"];

    var jobHead = document.createElement("div")
    jobHead.innerHTML =
        '<div style="width: 100%;">' +
        '   <div class="leftMenuEntryStyle" id="' + leftItemName + '">' +
        '   </div>' +
        '   <div style="rightMenuEntryStyle" id="' + rightItemName + '">' +
        '   </div>' +
        '</div>' +
        '<div style="clear:both"></div>';
    entry.appendChild(jobHead)

    var leftItem = document.getElementById(leftItemName);
    var rightItem = document.getElementById(rightItemName);

    rightItem.className += " logHeader"
    rightItem.style.margin = "0"
    rightItem.innerHTML = jobs[job]["name"]
    rightItem.style.cursor = "pointer"
    rightItem.onclick = function () {
        runJob(jobs[job], function (response) {
            log("Ran Job", response)
        });
    }

    leftItem.innerHTML = String.fromCharCode(0xf04b)
    leftItem.className += " logHeader"
    leftItem.style.fontFamily = "FontAwesome"
    leftItem.style.margin = "0"
    leftItem.style.cursor = "pointer"
    leftItem.onclick = function () {
        runJob(jobs[job], function (response) {
            log("Ran Job", response)
        });
    }


    var actionDiv = document.createElement("div")
    entry.appendChild(actionDiv)
    actionDiv.className = "table-responsive jobActions"
    actionDiv.style.width = "80%"

    var actionTable = document.createElement("table")
    actionDiv.appendChild(actionTable)
    actionTable.className = "table jobTable"
    actionTable.innerHTML =
        //'<h3 style="margin-top=0px;">Actions</h3>' +
        "<tr>" +
        "<th>Device</th>" +
        "<th>Action</th>" +
        "</tr>"

    var details = ""
    for (var iTask in jobs[job]["tasks"]) {
        var task = jobs[job]["tasks"][iTask]
        actionTable.innerHTML +=
            "<td>" + task["device"] + "</td>" +
            "<td>" + task["action"]["name"] + "</td>" +
            "</tr>"
    }

}

function addJobs(jobs) {
    for (var job in jobs) {
        createJob(job, jobs)
    }
}

function initDevices() {
    getDevices(function (response) {
        log("Available devices", response)
        response = sortObject(response)
        $.each(response, function (key, value) {
            addMenuEntry(value.name, value.icon, value)
        })

        _deviceLoaded = true;
    });
}


/**
 * Loads the available jobs
 */
function initJobs() {

    // the combobox containg the device selection
    var menuNav = document.getElementById(_deviceSelect);

    getJobs(function (response) {
        log("Available jobs", response);
        _jobsLoaded = true;
        addMenuEntry("Jobs", 0xf292, response)
    });
}



function addMenuEntry(name, icon, dev) {
    // create a new menu entry for the device
    // var a = document.createElement("a");
    var menuItem = document.createElement("li");
    menuItem.id = name;
    _menuNav.appendChild(menuItem);

    var leftItemName = "devNavLeft_" + name;
    var rightItemName = "devNavRight_" + name;

    var mainDiv =
        '<div class="menuEntry">' +
        '   <div class="leftMenuEntryStyle" id="' + leftItemName + '">' +
        '   </div>' +
        '   <div style="rightMenuEntryStyle" id="' + rightItemName + '">' +
        '   </div>' +
        '</div>' +
        '<div style="clear:both"></div>';

    menuItem.innerHTML = mainDiv;
    var leftItem = document.getElementById(leftItemName);
    var rightItem = document.getElementById(rightItemName);

    var devLink = document.createElement("a");
    devLink.fontFamily = "FontAwesome"
    devLink.style.fontSize = "22px";
    devLink.textContent = String.fromCharCode(parseInt(icon))
    devLink.setAttribute('href', "#" + name);
    leftItem.appendChild(devLink)

    devLink = document.createElement("a");
    devLink.style.paddingTop = 2;
    devLink.textContent = name;
    devLink.setAttribute('href', "#" + name);
    rightItem.appendChild(devLink)

    if (null != dev)
        _devices[name] = dev

    if (window.location.hash.substring(1) == dev.name) {
        selectionChanged()
    }
}

//
// register key press handler
//
$(document).keypress(function (e) {

    if (e.keyCode != 116) {
        e.preventDefault();
    }

    var code = ""

    switch (e.keyCode) {
        // left arrow
        case 37:
            code = "KEY_LEFT"
            break;
        case 38:
            code = "KEY_UP"
            break;
        case 39:
            code = "KEY_RIGHT"
            break;
        case 40:
            code = "KEY_DOWN"
            break;
        case 13:
            code = "KEY_ENTER"
            break;
        case 8:
            code = "KEY_BACK"
            break;
        default:
            var char = String.fromCharCode(e.which);
            switch (char.toLowerCase()) {
                case "w":
                    code = "KEY_VOLUMEUP"
                    break;
                case "s":
                    code = "KEY_VOLUMEDOWN"
                    break;
                case "d":
                    code = "KEY_RIGHT"
                    break;
                case "a":
                    code = "KEY_LEFT"
                    break;
                case "p":
                    code = "KEY_POWER"
                    break;
                case "q":
                    code = "KEY_MUTE"
                    break;
                case " ":
                    code = "KEY_PLAY"
                    break;

                case "0":
                case "1":
                case "2":
                case "3":
                case "4":
                case "5":
                case "6":
                case "7":
                case "8":
                case "9":
                    code = "KEY_" + char;
                    break;
            }
    }

    if (null != _selectedDevice) {
        for (var i = 0; i < _selectedDevice.keys.length; i++) {
            if (_selectedDevice.keys[i].name == code) {
                handleKey(_selectedDevice.keys[i]);
                break;
            }
        }
    }
});

//
// Handle a press on a key.
//
function handleKey(key, value) {
    sendToDevice(key, value, _selectedDevice, function (result) {
        log("key result", result)
    });
}

//
// Add a new button to the remote control div container.
//
function addButton(device, buttonType) {
    //Create an input type dynamically.
    var labels = getButtonLabels();
    var buttonInfo = labels[buttonType.name];

    // handle not supported buttons.
    if (null == buttonInfo || null == buttonInfo.button) {
        buttonInfo = labels["default"];
    }

    if (0 == buttonInfo.button.length) {
        log("button text is empty: " + buttonType.name);
    }

    var buttonLabel = buttonType.label;
    if ((null == buttonLabel || buttonLabel.length <= 0) && buttonLabel != "default")
        buttonLabel = buttonInfo.label


    // create the div container in which the button and label is saved.
    var buttonContainer = document.createElement("div");
    buttonContainer.id = "keyDiv"

    var inputButtonDiv = document.createElement("div")
    inputButtonDiv.className = "inputContainer"
    inputButtonDiv.id = device.name +
        buttonType.group + buttonType.name

    var inputbutton = document.createElement("input");
    inputButtonDiv.appendChild(inputbutton)
    buttonContainer.appendChild(inputButtonDiv); // add to main div

    inputbutton.className = "largeInput";
    inputbutton.value = buttonInfo.button;
    inputbutton.name = buttonType.name;

    inputbutton.style.color = buttonInfo.color;
    inputbutton.style.fontFamily = buttonInfo.font;

    // otherwise value from css is used
    if (buttonInfo.fontSize > 0)
        inputbutton.style.fontSize = buttonInfo.fontSize;

    // add a new description label.
    var inputLabel = document.createElement("label");
    inputLabel.className = "largeInputLabel"
    inputLabel.style.cursor = "pointer";
    inputLabel.innerHTML = buttonLabel;
    buttonContainer.onmouseover = function () {
        // inputbutton.style.background = "#424242"
    };

    buttonContainer.onmouseout = function () {
        // inputbutton.style.background = "#212121"

    };

    // add it to our div.
    buttonContainer.appendChild(inputLabel);

    var container = getButtonGroup(buttonInfo, buttonType)

    // Append the element in page (in span).
    container.appendChild(buttonContainer);

    // add the handling of the button
    // todo find a cleaner way to add this button
    if (null == buttonType.type || buttonType.type == "default") {
        buttonContainer.type = "button";
        inputbutton.type = "button";
        buttonContainer.onclick = function () {
            handleKey(buttonType);
            // removes the focus from the button
            buttonContainer.blur()
        };
    } else {
        if (buttonType.type == "color") {
            var colorCookie = "color" +
                buttonType.group +
                buttonType.code

            inputbutton.type = "color"
            inputbutton.style.marginLeft = 0

            getButtonValue(_selectedDevice, buttonType, function (response) {
                rgb = response.value
                hex = rgbToHex(rgb[0], rgb[1], rgb[2])
                inputbutton.value = hex
            });

            _value_callbacks[inputButtonDiv.id] = function (key) {
                rgb = key.value
                hex = rgbToHex(rgb[0], rgb[1], rgb[2])
                inputbutton.value = hex
            }

            buttonContainer.onchange = function () {
                rbgColor = hexToRgb(inputbutton.value)
                handleKey(buttonType, rbgColor)
                setCookie(colorCookie, inputbutton.value, 9999 /* life very long*/);
                inputbutton.blur()
            };
        } else if (buttonType.type == "toggle") {
            inputId = uniqueNumber()
            inputButtonDiv.style.paddingTop = "5px"
            inputButtonDiv.innerHTML =
                '<label class="switch">' +
                '<input type="checkbox" id="' + inputId + '">' +
                '<div class="slider round"></div>' +
                '</label>'

            var togglectrl = document.getElementById(inputId)
            togglectrl.onclick = function () {
                handleKey(buttonType, togglectrl.checked);
                // removes the focus from the button
                buttonContainer.blur()
            };

            getButtonValue(_selectedDevice, buttonType, function (response) {
                togglectrl.checked = response.value
            });

            _value_callbacks[inputButtonDiv.id] = function (key) {
                togglectrl.checked = key.value
            }

        } else if (buttonType.type == "slider") {
            var oldvalue = buttonType.value;
            var initalized = false;

            var slider = $(inputbutton).rangeslider({

                // Feature detection the default is `true`.
                // Set this to `false` if you want to use
                // the polyfill also in Browsers which support
                // the native <input type="range"> element.
                polyfill: false,

                // Callback function
                onInit: function () { },

                // Callback function
                onSlideEnd: function (position, value) {
                    if (!initalized)
                        return

                    if (oldvalue == value)
                        return

                    _ignore_swipe = true;
                    oldvalue = value
                    handleKey(buttonType, value)
                },

                // Callback function
                onSlide: function (position, value) {
                }
            });

            _value_callbacks[inputButtonDiv.id] = function (key) {
                initalized = false
                slider.val(key.value).change();
                initalized = true
            }

            getButtonValue(_selectedDevice, buttonType, function (response) {
                slider.val(response.value).change();
                initalized = true
            });


            //slider.max(100);
            inputButtonDiv.style.width = 186
            inputButtonDiv.style.paddingTop = 1
            inputbutton.type = "range"
            inputbutton.max = 100
            inputbutton.min = 0
            inputbutton.step = 5
            slider.rangeslider('update', true);
        } else {
            log("Button keytype not supported: " + buttonType.type);
        }
    }
}

function getButtonGroup(buttonInfo, button) {

    var grouabel = "";
    if (null != button.group && button.group.length > 0 && button.group != "default") {
        groupLabel = button.group
    } else {
        groupLabel = buttonInfo.group
    }

    var container = document.getElementById(groupLabel);

    if (null == container) {

        container = document.createElement("div");
        container.className = "col-lg-12 largeMargin"
        container.id = groupLabel

        var header = document.createElement("div");
        header.innerHTML = groupLabel
        header.className = "groupLabel"
        container.appendChild(header)


        var locationInfo = getNextGroup(groupLabel)

        // this will be inserted before anything.
        if (locationInfo.before == locationWildCard) {

            // no first child yet do insert. 
            if (null == _mainButtonContainer.firstChild) {
                _mainButtonContainer.appendChild(container)
            } else {
                // insert before the first item.
                _mainButtonContainer.insertBefore(container, _mainButtonContainer.firstChild)
            }
        } else if (locationInfo.after == locationWildCard) { // insert as last item.
            // no first child yet do insert. 
            if (null == _mainButtonContainer.lastChild) {
                _mainButtonContainer.appendChild(container)
            } else {
                // insert before the first item.
                insertAfter(container, _mainButtonContainer.lastChild)
            }
        } else {
            var insertBeforeElement = document.getElementById(locationInfo.before)
            var insertAfterElement = document.getElementById(locationInfo.after)
            var itterations = 0;
            var maxItterations = 100;
            // we have to find the next possible element which already 
            // exists in the dom 
            while (null == insertBeforeElement &&
                locationInfo.before != locationWildCard &&
                locationInfo.after != locationWildCard) {
                locationInfo = getNextGroup(locationInfo.before)
                insertBeforeElement = document.getElementById(locationInfo.before)
                if (++itterations > maxItterations) {
                    alert("Failed to find group")
                    break;
                }
            }

            while (null == insertAfterElement &&
                locationInfo.after != locationWildCard &&
                locationInfo.before != locationWildCard) {
                locationInfo = getNextGroup(locationInfo.after)
                insertAfterElementj = document.getElementById(locationInfo.after)
                if (++itterations > maxItterations) {
                    alert("Failed to find group")
                    break;
                }
            }

            if (null != insertBeforeElement) {
                _mainButtonContainer.insertBefore(container, insertBeforeElement)
            } else if (null != insertAfterElement) {
                insertAfter(container, insertAfterElement)
            } else {
                _mainButtonContainer.appendChild(container)
            }
        }
    }

    return container;

}

function socketDataReceived(event) {
    device = JSON.parse(event.data)
    device.keys.forEach(function (key) {
        button_id = device.name +
            key.group + key.name
        if (null != _value_callbacks[button_id]) {
            _value_callbacks[button_id](key)
        }


    }, this);

    /*{
    "name": "Light",
    "icon": 61675,
    "keys": [
        {
            "type": "default",
            "name": "KEY_POWER2",
            "group": "All",
            "label": "On",
            "can_read": false,
            "value": null,
            "code": "on"
        }
    ]
    }
    */
}

//
// Function to log data into the log panel
// Date and time is appended automatically.
//
function log(description, jsonData = null) {
    var logBox = document.getElementById('logText');
    if (null == logBox)
        return;

    var entry = document.createElement("div")
    entry.className = "logEntry"

    var logHead = document.createElement("div")
    logHead.className = "logHeader"
    logHead.innerHTML = "Log Entry " + new Date().toLocaleString()
    entry.appendChild(logHead)

    var logDesc = document.createElement("div")
    logDesc.className = "logDesc"
    logDesc.innerHTML = '<h1>Description</h1><br>' + "<p>" + description + "</p>"
    entry.appendChild(logDesc)


    logText = description;
    if (null != jsonData) {
        var logJson = document.createElement("div")
        logJson.className = "logDesc"

        var jsonDesc = document.createElement("h1")
        jsonDesc.innerHTML = "Method data"
        logJson.appendChild(jsonDesc)

        logJson.appendChild(renderjson(jsonData))
        entry.appendChild(logJson)
    }

    logBox.insertBefore(entry, logBox.firstChild);
}


