var _rest_url = "http://" + window.location.host + "/rest/";
var _socket_url = "ws://" + window.location.host + "/ws/";

var _device_resource = "devices/"
var _system_resource = "system/"
var _job_resource = "jobs/"


var _deviceIoSocket;

function sendToDevice(key, value, device, responseCallback) {
	// clone device.
	dev = JSON.parse(JSON.stringify(device))
	dev.keys = [key]
	
	var json = new Object();
	json.device = dev;

	if (null != value) {
		key.value = value
	}

	var url = _rest_url + _device_resource + device.name
	httpPostJson(json, responseCallback, url);
}

//
// asks the server via a json request for the device list.
//
function getDevices(responseCallback) {
	var url = (_rest_url + _device_resource);
	httpGetJson(url, responseCallback);
}

function getButtonValue(device, key, responseCallback) {
	var url = _rest_url + _device_resource
		+ device.name + "/"
		+ key.group + "/"
		+ key.name;
	httpGetJson(url, responseCallback);
}

function getJobs(responseCallback) {
	var url = (_rest_url + _job_resource);
	httpGetJson(url, responseCallback);
}

function runJob(job, responseCallback) {
	var json = new Object();
	json.name = job.name;
	json.arguments = null

	var url = _rest_url + _job_resource + job.name
	httpPostJson(json, responseCallback, url);
}

//
// Reload the vidar configuration.
//
function reloadVidarConfig() {
	var json = new Object();
	json.name = "reload";
	json.arguments = null;

	var url = _rest_url + _system_resource + json.name
	httpPostJson(json, function (response) {
		log("Reloaded configuration", response)
	}, url);
}

function httpGetJson(url, responseCallback) {
	httpGet(url, function (data) {
		var jsonData = JSON.parse(data)
		responseCallback(jsonData)
	});
}

function httpGet(url, responseCallback) {
	$.ajax({
		url: url,
		type: "GET",
		contentType: 'application/json; charset=utf-8',
		success: function (data) {
			responseCallback(data)
		}
	});
}

function httpPostJson(data, responseCallback, url) {
	var jsondata = JSON.stringify(data)
	log("HTTP POST json", data)
	$.ajax({
		type: 'POST',
		url: url,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		data: jsondata,
		success: function (data) {
			responseCallback(data);
		}   // Success Function
	}); // AJAX Call
}

function openSocket(dataReceivedcallback) {
	_deviceIoSocket = new WebSocket(_socket_url);
	//_deviceIoSocket.onmessage = function(event){alert(event.data)};
	_deviceIoSocket.onmessage = dataReceivedcallback;
}
