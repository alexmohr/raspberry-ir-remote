"""
    LIRC Integration. Control IR device with lirc
"""

import subprocess
import os
import pathlib
import fnmatch
from config import Device, Key
from plugins import Baseplugin

class LircCfg:
    """ Configuration for LIRC plugin """

    # default path to the lirc configuration
    config_path = '/etc/lirc/'
    main_config_file = 'lircd.conf'

    list_devices = "LIST"
    send_once = "SEND_ONCE"

    ##
    # Following all strings which are used to parse the configuration.
    ##

    # tells lirc to read other configuration files.
    # !\todo maybe they should be moved to a configuration file.
    cfg_include = 'include'
    cfg_device_begin = 'begin remote'
    cfg_device_end = 'end remote'
    cfg_device_name = 'name'

    cfg_keys_begin_raw = "begin raw_codes"
    cfg_keys_begin = 'begin codes'
    cfg_keys_end = 'end codes'
    cfg_keys_end_raw = 'end raw_codes'

class LircIo(Baseplugin):
    """ LIRC plugin for infrared devices """

    def __init__(self):
        super(LircIo, self).__init__()
        self.devices = []

    def get_key_state(self, dev, dev_key):
        return dev_key

    def find_device(self, name):
        """ List all LIRC devices. """

        for dev in self.devices:
            if dev.name == name:
                return dev
        raise ValueError('Could not find device: ' + name)

    # main entry when parsing configuration.
    def configure_plugin(self):
        """ collect the actually configured devices. """
        self.devices = self.parse_config(self.cfg["lirc"]["config"])

    def find_devices(self):
        """ Return available devices """
        return self.devices

    def parse_config(self, file):
        """ Parse the lirc configuration. """

        # open the lirc configuration file.
        with open(file) as cfg:
            reading_device = False
            reading_keys = False

            # itterate over each line in the file.
            for line in cfg:

                # ignore comments
                if line.startswith('#'):
                    continue

                # New device
                if LircCfg.cfg_device_begin in line:

                    # handle nested devices
                    if reading_device:
                        raise ValueError('Nested devices are not possible!')

                    # hold the info that we are reading a device.
                    reading_device = True

                # done reading device.
                # we have to do this if more than 1 device per c
                # configuration file exists.
                elif LircCfg.cfg_device_end in line:
                    reading_device = False

                elif LircCfg.cfg_keys_begin in line or \
                        LircCfg.cfg_keys_begin_raw in line:
                    # handle nested keys
                    if reading_keys:
                        raise ValueError('Nested keys are not possible!')
                    else:
                        reading_keys = True

                elif LircCfg.cfg_keys_end in line or \
                        LircCfg.cfg_keys_end_raw in line:

                    reading_keys = False

                # Handle configuration of device
                elif reading_device and not reading_keys:
                    if LircCfg.cfg_device_name in line:
                        # get the name of the device
                        name = line.split()[1]

                        # create a new instance of device and add it to self.
                        my_device = Device(name)
                        if name in self.cfg["devices"]:
                            my_device.icon = \
                                self.cfg["devices"][name]["menu_icon_fa"]

                        self.devices.append(my_device)

                # handle reading of keys
                elif reading_keys and not line.isspace():
                    key_details = line.split()

                    if len(key_details) >= 2:
                        if "name" in key_details[0]:
                            # this is the case for raw keys.)
                            my_key = Key(key_details[1], "<raw>")
                            my_device.keys.append(my_key)
                        elif not key_details[0].isdigit():
                            key_name = key_details[0]
                            key_code = key_details[1]
                            my_key = Key(key_name, key_code)
                            my_device.keys.append(my_key)
                            # my_key.name = key_name
                            # my_key.code = key_code
                else:
                    if LircCfg.cfg_include in line:
                        # defines how file find pattern
                        pattern = line.split()[1]

                        # remove " from the string. we do not need them.
                        pattern = pattern.replace('"', "")
                        path = pathlib.Path(pattern)

                        absolute_path = None
                        if path.is_absolute():
                            absolute_path = path
                        else:
                            # path of the current parsed file.
                            current_file_path = pathlib.Path(file)

                            # the last argument forces path.join to add another
                            absolute_path = os.path.join(
                                str(current_file_path.parent),
                                str(path.parent), '')

                        # parse each file which matches the pattern.
                        for file in os.listdir(absolute_path):
                            if fnmatch.fnmatch(file, path.name):
                                config = os.path.join(absolute_path, file)
                                self.parse_config(config)

            # return all device we have found.
            return self.devices

    def run_lirc(self, command, *params):
        """ Run a command to with IR send """
        command = "irsend " + command
        for param in params:
            command += " " + param

        result = subprocess.getoutput(command)
        return result

    # lirc does not care about the value.
    def handle_key(self, dev_key, dev):
        """ Handle the key request """
        output = self.run_lirc(LircCfg.send_once, dev.name, dev_key.name)
        dev_key.value = output
