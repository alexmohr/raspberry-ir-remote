"""
    Module for hyperion integration.
"""

import logging
import socket
import re
import jsonpickle
import spur

import config
from config import Device, Key
from plugins import Baseplugin

__logger__ = logging.getLogger(__name__)


class HyperCfg:
    """
        Configuration for hyperion device.
    """
    color_wheel_button = "BTN_WHEEL"
    cmd_color = "color"
    cmd = "command"


class Hyperion(Baseplugin):
    """
        Hyperion integration.
    """

    def __init__(self):
        super(Hyperion, self).__init__()

        self.hyperion_socket = None
        self.hyperion_shell = None
        self.is_open = False
        self.devices = []
        self.open = False
        self.color = [0, 0, 0]

    def get_key_state(self, dev, dev_key):
        """ Gets the state of the key """
        if dev_key.type == "color":
            dev_key.value = self.color
        return dev_key

    def configure_plugin(self):
        """ Parse the configuration file"""

        my_device = Device(self.cfg['menu_name'])
        my_device.icon = 0xf135

        self.devices.append(my_device)

        for cfg_key in self.cfg['keys']:
            my_key = config.create_key_instance(cfg_key)
            my_device.keys.append(my_key)

        self.open_connection()

    def find_devices(self):
        """ Return the devices in this device """
        return self.devices

    def handle_key(self, dev_key, dev):
        """ Handle the given key for hyperion """

        # protocoll
        # https://github.com/AlexisBRENON/hyperion2boblight/blob/773aadf51e25e5117f39db53bf1cc81018d6dc81/communication%20protocol.json
        # {
        #    "color":[int, int, int],
        #    "cmd":"color",
        #    "priority":int
        # }
        # http://stackoverflow.com/questions/21.33340/sending-string-via-socket-python
        cmd = {}
        dev.keys.append(dev_key)
        if ("RED" in dev_key.name or
                "GREEN" in dev_key.name or
                "BLUE" in dev_key.name or
                "YELLOW" in dev_key.name or
                "POWER" in dev_key.name or
                "color" in dev_key.type):

            cmd[HyperCfg.cmd] = "color"
            if "color" == dev_key.type:
                if type(dev_key.value) is str:
                    user_in = re.sub('[!@#$]', '', dev_key.value)
                    rgb = user_in.split(',')
                    color_data = []
                    for color in rgb:
                        color_data.append(int(color))

                    cmd[HyperCfg.cmd_color] = color_data
                else:
                    cmd[HyperCfg.cmd_color] = dev_key.value
            else:
                cmd[HyperCfg.cmd_color] = dev_key.value

            dev_key.name = "Color"
            dev_key.type = "color"
            self.color = dev_key.value

        elif "CLEAR" in dev_key.name:
            dev_key.name = "Color"
            dev_key.type = "color"
            dev_key.value = [0, 0, 0]
            cmd[HyperCfg.cmd] = "clear"

        elif "RESTART" in dev_key.name:
            self.hyperion_shell.run(["systemctl", "restart", "hyperion"])
            return  # skip sending of json.

        cmd['priority'] = self.cfg['priority']
        payload = jsonpickle.json.dumps(cmd)
        payload += "\n"  # TERMINATION CHAR! VERY IMPORTANT!

        __logger__.debug("Hyperion request:")
        __logger__.debug(payload)
        try:
            self.hyperion_socket.send(payload.encode())
        except BrokenPipeError:
            self.open = False

            # reopen connection and try again
            self.open_connection()

            if self.is_open:
                self.handle_key(dev_key, dev)

    def open_connection(self):
        """ Connect to the hyperion server. """

        self.hyperion_socket = socket.socket(
            socket.AF_INET,
            socket.SOCK_STREAM)

        __logger__.debug(
            "hyperion: connecting to socket " +
            self.cfg['host'] +
            ":" + str(self.cfg['port']))

        self.hyperion_socket.connect((self.cfg['host'], self.cfg['port']))

        # used to restart hyperion.
        self.hyperion_shell = spur.SshShell(
            hostname=self.cfg['host'],
            username=self.cfg['ssh_user'],
            password=self.cfg['ssh_passwd'],
            missing_host_key=spur.ssh.MissingHostKey.accept)

        self.is_open = True
