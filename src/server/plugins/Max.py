"""
    MAX! integration
"""
import logging

from config import Device, Key
from plugins import Baseplugin

from maxcube.cube import MaxCube
from maxcube.connection import MaxCubeConnection

__logger__ = logging.getLogger(__name__)

class MaxCubeDevice(Baseplugin):
    """ MAX! Cube plugin"""

    def __init__(self):
        super(MaxCubeDevice, self).__init__()
        self.is_open = False
        self.devices = []
        self.cube = None
        self.thermostats = {}

    def get_key_state(self, dev, dev_key):
        """ Gets the state of the given device """
        return dev_key

    def configure_plugin(self):
        """ Configure MAX! plugin """
        return

        my_device = Device(self.cfg['menu_name'])
        my_device.icon = 0xf2c9

        self.devices.append(my_device)

        self.cube = MaxCube(
            MaxCubeConnection(self.cfg['host'], self.cfg['port']))
        self.cube.log()

        for dev in self.cube.devices:
            self.thermostats[dev.name] = dev

            my_key = Key("KEY_POWER", "Power")
            my_key.group = dev.name
            my_device.keys.append(my_key)

            my_key = Key(
                "STAT_TEMPERATURE",
                "Current " + str(dev.actual_temperature) + "°")
            print(my_key.code)

            my_key.group = dev.name
            my_device.keys.append(my_key)

            my_key = Key(
                "STAT_TEMPERATURE",
                "Set\n" + str(dev.target_temperature) + "°")

            my_key.group = dev.name
            my_device.keys.append(my_key)

    def find_devices(self):
        """ Return all thermostats """
        return []
        return self.devices

    def handle_key(self, dev_key, dev):
        if "POWER" in dev_key.name:
            __logger__.debug("opop")
            thermostat = self.thermostats[dev_key.group]
            self.cube.set_target_temperature(thermostat, 50)
