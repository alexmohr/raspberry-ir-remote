"""
    Module for demonstration and test plugin.
"""

import logging

# Import error can be ignored. Will be set by by application
from config import Device
from plugins import Baseplugin

import config
from config import Device, Key

__logger__ = logging.getLogger(__name__)

class Demo(Baseplugin):
    """ Provides a demo device for testing functions and api verfication """

    def __init__(self):
        super(Demo, self).__init__()
        self.devices = []
        self.brigthness = 0
        self.power = False
        self.color = [0, 0, 0]

    def get_key_state(self, dev, dev_key):
        if dev_key.code == "color":
            dev_key.value = self.color
        elif dev_key.code == "TogglePower":
            dev_key.value = self.power
        elif dev_key.code == "brightness":
            dev_key.value = self.brigthness
        return dev_key

    def configure_plugin(self):
        """ Parses the configuration file in a plugin."""
        # create a new device
        my_device = Device("Demo")
        my_device.icon = 0xf0c3
        for cfg_key in self.cfg["keys"]:
            key = config.create_key_instance(cfg_key)
            my_device.keys.append(key)

        self.devices.append(my_device)

    def find_devices(self):
        """ Returns the devices of this class """
        return self.devices

    def handle_key(self, dev_key, dev):
        if dev_key.code == "color":
            self.color = dev_key.value
        elif dev_key.code == "TogglePower":
            self.power = dev_key.value
        elif dev_key.code == "brightness":
            self.brigthness = dev_key.value
