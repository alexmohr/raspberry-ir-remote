""" Module for LimitlessLED / Milight """
import logging
import milight
import copy

import config
from config import Device, Key
from plugins import Baseplugin

__logger__ = logging.getLogger(__name__)

__max_brightness__ = 100
__min_brightness__ = 0


class Milight(Baseplugin):
    """ Communication with milight wifi brige """

    def __init__(self):
        super(Milight, self).__init__()
        self.light = None
        self.controller = None
        self.states = {}
        self.zones = {}
        self.brightness = []
        self.color = []
        self.power = []
        self.devices = []

    def find_devices(self):
        """
            Return a collection with all devices this plugin supports
        """

        return self.devices

    def configure_plugin(self):
        """
            Parses the given configuration file. The file can be configured via the
        """
        my_device = Device(self.cfg['menu_name'])
        my_device.icon = 0xf0eb

        # single device
        self.devices.append(my_device)

        for cfg_zone in sorted(self.cfg["zones"].items(), key=lambda x: x[1]["id"]):
            #zone = self.cfg["zones"][cfg_zone]
            zone = cfg_zone[1]
            zone_name = zone['label']
            zone_id = zone['id']
            self.zones[zone_name] = zone_id
            for cfg_key in zone["keys"]:
                dev_key = config.create_key_instance(cfg_key)
                dev_key.group = zone_name
                my_device.keys.append(dev_key)

        # even sorting is fun in python :)
        # sort fields are persistent
        #my_device.keys.sort(key=lambda x: x.label)
        #my_device.keys.sort(key=lambda x: x.group)
        #my_device.keys.sort(key=lambda x: x.name, reverse=True)

        self.open_connection()
        self.list_keys(my_device)

    def open_connection(self):
        """ Connect to the wifi brige """

        # Create a controller with 0 wait between commands
        self.controller = milight.MiLight(
            {'host': self.cfg['host'], 'port': 8899}, wait_duration=0)
        self.light = milight.LightBulb(['rgbw', 'white', 'rgb'])

        for i in range(0, len(self.zones)):
            self.brightness.append(__max_brightness__)
            self.color.append([255, 255, 255])
            self.power.append(True)

        # initalize with default values.
        if self.cfg["init"] == True:
            self.controller.send(self.light.all_on())
            self.controller.send(self.light.brightness(__max_brightness__, 0))
            self.controller.send(self.light.white(0))

    def handle_key(self, dev_key, dev):
        """
            Handle the given key for the device and return a result.
        """
        zone_id = self.zones[dev_key.group]
        if dev_key.code == "brightness":
            self.controller.send(self.light.brightness(dev_key.value, zone_id))

            if zone_id == 0:
                for i in range(0, len(self.zones)):
                    self.brightness[i] = dev_key.value
                    self.add_zone_key_value(dev_key, i, dev)
            else:
                self.brightness[zone_id] = dev_key.value
                dev.keys.append(dev_key)
            self.brightness[zone_id] = dev_key.value

            self.enable_power(dev_key, dev)
            
        elif dev_key.code == "color":
            self.controller.send(self.light.color(
                milight.color_from_rgb(
                    dev_key.value[0], dev_key.value[1], dev_key.value[2]), zone_id))

            if zone_id == 0:
                for i in range(0, len(self.zones)):
                    self.color[i] = dev_key.value
                    self.add_zone_key_value(dev_key, i, dev)
            else:
                self.color[zone_id] = dev_key.value
                dev.keys.append(dev_key)    
            self.enable_power(dev_key, dev)

        elif dev_key.code == "white":
            self.controller.send(self.light.white(zone_id))
            if zone_id == 0:
                for i in range(0, len(self.zones)):
                    self.color[i] = [255, 255, 255]
                    self.add_zone_key_value(dev_key, i, dev)
            else:
                self.color[zone_id] = [255, 255, 255]
                dev.keys.append(dev_key)

            color_key = Key("Color", "color")
            color_key.group = dev_key.group
            color_key.value = [255, 255, 255]
            self.handle_key(color_key, dev)
            
            #slf.enable_power(dev_key, dev)

        elif dev_key.code == "power_toggle":
            is_on = dev_key.value
            #is_on = not self.power[zone_id]
            dev_key.value = is_on
            if is_on:
                self.controller.send(self.light.on(zone_id))
            else:
                self.controller.send(self.light.off(zone_id))
            
            if zone_id == 0:
                for i in range(0, len(self.zones)):
                    self.power[i] = is_on
                    self.add_zone_key_value(dev_key, i, dev)
            else:
                self.power[zone_id] = is_on
                dev.keys.append(dev_key)

    def enable_power(self, dev_key, dev):
        power_key = Key("KEY_POWER", "power_toggle")
        power_key.group = dev_key.group
        power_key.value = True
        self.handle_key(power_key, dev)
        
    def add_zone_key_value(self, dev_key, i, dev):
        zone_key = copy.deepcopy(dev_key)
        zone_key.group = list(self.zones)[i]
        dev.keys.append(zone_key)

    def get_key_state(self, dev, dev_key):
        """ Returns the status of the given key """

        zone_id = self.zones[dev_key.group]
        if str.lower(dev_key.name) == "brightness":
            dev_key.value = self.brightness[zone_id]
        elif str.lower(dev_key.name) == "color":
            dev_key.value = self.color[zone_id]
        elif "power" in str.lower(dev_key.name):
            dev_key.value = self.power[zone_id]

        return dev_key
