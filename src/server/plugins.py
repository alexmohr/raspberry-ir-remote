"""
    Module for basic plugin functions
"""

class Baseplugin:
    """ Base class for devices.Implement it to create a plugin """
    def __init__(self):
        self.devices = []
        self.cfg = None

    def find_devices(self):
        """
        Return a collection with all devices this plugin supports
        """
        raise NotImplementedError("find devices not implemented")

    def handle_key(self, dev_key, dev):
        """ Handles the given key for the device """
        raise NotImplementedError("handle key not implemented")

    def configure_plugin(self):
        """
        Parses the given configuration file. The file can be configured via the
        """
        raise NotImplementedError("parse config not implemented")

    def get_key_state(self, dev, dev_key):
        """
        Returns the state of the given key for the device
        """
        return dev_key

    def list_keys(self, dev):
        """ List the keys of the given device """
        keys = []
        for my_dev in self.devices:
            if my_dev.name != dev.name:
                continue

            for dev_key in my_dev.keys:
                dev_key = self.get_key_state(my_dev, dev_key)
                keys.append(dev_key)
        return keys
