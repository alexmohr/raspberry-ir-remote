# -*- coding: utf-8 -*-
""" Parses command and is the layer between the web and the devices """

import logging
import os
import sys
import copy

import jsonpickle
from pluginbase import PluginBase

# config imports.
import config
from config import *

__logger__ = logging.getLogger(__name__)


class CommandParser:
    """ Handles the devices plugins and command io """

    def __init__(self):
        self.cfg = None
        # create a dictonary with the devices.
        # key will be the name of the device.
        # value will be an array
        # index 0 will be the instance of the device class.
        # index 1 will be the instance of the handler class.
        self.devices = {}

        # create a list with jobs.
        # a jobs is a combination of actions a device will execute
        self.jobs = {}

        # contains system actions like cfg reload
        self.actions = {}

        # Create a “plugin base” object.
        # It defines a pseudo package under which all your plugins
        # will reside.
        self.plugin_base = PluginBase(package=Plugins.plugin_package)

        # The plugin related stuff must be hold as class refernce.
        # Otherwise the GC will collect these objects.
        # This leads to the problem that the imports in the plugins
        # would not work anymore.

        # Now that you have a plugin base,
        # you can define a plugin source which is the list of
        # all sources which provide plugins:
        self.plugin_source = self.plugin_base.make_plugin_source(
            searchpath=[Plugins.plugin_path])

    def load_config(self):
        """ Load the plugin configuration """

        self.devices.clear()
        self.jobs.clear()

        # parse the configuration file.
        plg_cfg = Plugins.config_file
        with open(plg_cfg, 'r') as config_file:
            content = config_file.read()
            self.cfg = jsonpickle.decode(content)

        # To import a plugin all you need to do is to use the regular
        # import system. The only change is that you need to import
        # the plugin source through the with statement:

        # iterate over each plugin in the configuration
        for plugin in self.cfg['plugins']:
            try:
                # load the plugin module
                plugin_module = \
                    self.plugin_source.load_plugin(plugin['module_name'])

                if plugin_module == "Baseplugin":
                    continue

                # init the class if necessary
                instance = None
                if 'class_name' in plugin:
                    class_ = getattr(plugin_module, plugin['class_name'])
                    instance = class_()
                else:
                    # otherwise just load the module
                    instance = plugin_module

                # parse the configuration of the plugin.
                plugin_config_file = \
                    os.path.join(os.path.dirname(
                        sys.argv[0]), plugin['config'])

                with open(plugin_config_file, 'r') as config_file:
                    content = config_file.read()
                    decoded_configuration = jsonpickle.decode(content)

                    instance.cfg = decoded_configuration
                    instance.configure_plugin()

                for plugin_device in instance.find_devices():

                    # get the devies.
                    self.devices[plugin_device.name] = \
                        [plugin_device, instance]

            # pylint: disable=W0703
            except Exception:
                msg = "Failed to load plugin " + plugin['class_name']
                __logger__.exception(msg)

            self.add_wildcard_device()

            # add jobs
            for job in self.cfg["jobs"]:
                self.jobs[job["name"]] = job

            # add system actions
            self.actions = {
                "reload": self.action_reload
            }

    def add_wildcard_device(self):
        """ Adds a wildcard device which contains all common keys."""
        my_device = Device(ConstantValues.wildcard)
        my_device.icon = 0xf069

        all_keys = {}
        for dev in self.devices:
            # make sure we count the buttons from the device only once.
            dev_keys = {}
            for dev_key in self.devices[dev][0].keys:
                if dev_key.name in dev_keys:
                    continue

                dev_keys[dev_key.name] = dev_key.name

                if dev_key.name not in all_keys:
                    all_keys[dev_key.name] = [dev_key, 1]
                else:
                    all_keys[dev_key.name][1] += 1

        for dev_key in all_keys:
            if all_keys[dev_key][1] >= len(self.devices):
                global_key = all_keys[dev_key][0]
                my_device.keys.append(global_key)

        self.devices[my_device.name] = [my_device, None]

    def run_job(self, job):
        """ Runs the given job """

        # return value
        ret_val = {}

        for task in job["tasks"]:
            key_instance = config.create_key_instance(task["action"])

            # value = 0
            # if "value" in task["action"]:
            #    value = task["action"]["value"]

            dev_name = task["device"]
            try:
                dev = self.devices[dev_name]
                handler = dev[1]
                res = handler.handle_key(dev_key=key_instance, dev=dev[0])
                if None is not res:
                    ret_val[dev_name] = [res, True]
                else:
                    ret_val[dev_name] = [key_instance, False]

            # pylint: disable=W0703
            except Exception as ex:
                __logger__.error(ex)
                ret_val[dev_name] = [key_instance, False]

        return jsonpickle.dumps(ret_val)

    def get_devices(self, path):
        """ Returns all configured devices """

        if len(path) == 1:
            all_devices = {}
            for my_device in self.devices:
                all_devices[my_device] = self.devices[my_device][0]
                if self.devices[my_device][1] is not None:
                    updated_keys = self.devices[my_device][1].list_keys(
                        dev=all_devices[my_device])

                    if updated_keys is not None:
                        all_devices[my_device].keys = updated_keys

            return jsonpickle.encode(all_devices, unpicklable=False)
        elif len(path) == 2:
            if path[1] not in self.devices:
                raise KeyError("device does not exist")

            dev = self.devices[path[1]]
            return jsonpickle.encode(dev[0], unpicklable=False)
        elif len(path) == 4:
            dev_hdlr = self.devices[path[1]]
            dev = dev_hdlr[0]
            handler = dev_hdlr[1]
            group_name = path[len(path) - 2]
            key_name = path[len(path) - 1]

            for key_instance in dev.keys:
                if (key_instance.group == group_name and
                        key_instance.name == key_name):
                    key_instance = handler.get_key_state(dev, key_instance)
                    result = jsonpickle.encode(key_instance, unpicklable=False)
                    __logger__.error(result)
                    return result
        raise ValueError("device resouce cannot be found")

    def post_devices(self, dev_name, request_json):
        """ Handle posts to devices """
        dev_hdlr = self.devices[dev_name]
        dev = dev_hdlr[0]
        handler = dev_hdlr[1]

        dev_cpy = copy.deepcopy(dev)
        dev_cpy.keys.clear()

        for dev_key in request_json["device"]["keys"]:
            key_instance = create_key_instance(dev_key)
            handler.handle_key(key_instance, dev_cpy)

            # dev_cpy.keys.append(key_instance)

        return jsonpickle.encode(dev_cpy, unpicklable=False)

    def get_jobs(self, path):
        """ Handle gets to jobs """
        return jsonpickle.encode(self.cfg["jobs"], unpicklable=False)

    def post_jobs(self, job, request_json):
        """ Handle posts to jobs """
        if not job in self.jobs:
            raise ValueError("A job with name \"" + job + "\" does not exist")

        return self.run_job(self.jobs[job])

    def get_system(self, params):
        """ Handle gets to system this contains commands like reload """
        return jsonpickle.encode(self.actions, unpicklable=False)

    def post_system(self, action, request_json):
        """ Handles posts to system resource """
        return self.actions[action]()

    def action_reload(self):
        """ System actions to reload the config """
        self.load_config()
        return None
