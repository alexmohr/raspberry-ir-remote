"""
    Contains configuration and static options.
"""

from enum import Enum
import os
import sys


class Arguments:
    """ Contains Arguments for parsing json files"""
    key = "key"
    value = "value"
    device = "device"
    list_keys = "listkeys"
    list_devices = "listdevices"
    list_jobs = "listjobs"
    state = "state"
    returncode = "return"
    reload = "reload"
    result = "result"
    runjob = "runjob"


class Buttons:
    """ Provide button options """
    color_wheel = "BTN_WHEEL"


class FileType(Enum):
    """
        defines the supported filenames
        which we read from subfolders.
    """
    css = "css"
    html = "html"
    javascript = "js"


class ButtonType(Enum):
    """
        Defines different types of buttons.
    """
    color = "color"
    slider = "slider"
    toggle = "toggle"
    default = "default"


class ConstantValues:
    """ Constant values """
    wildcard = "Wildcard"
    jobsDevice = "Jobs"


class Plugins:
    """ Plugin options """
    config_path = os.path.join(
        os.path.dirname(sys.argv[0]), "..", "cfg", "vidar")

    main_config_file = "plugin_config.json"
    config_file = os.path.join(config_path, main_config_file)

    plugin_package = 'commandparser'
    plugin_path = os.path.join(os.path.dirname(sys.argv[0]), 'plugins')


class Networking:
    """ Networking options """
    protcoll = 'http'
    default_host = "127.0.0.1"
    default_port = '8080'
    default_connection = protcoll + '://' + default_host + ':' + default_port


class Key:
    """ 
        Defines a key for a device
        Name: Kind of an ID for the key.
        Code: Can be freely used.
        Group: Group where the key is in.
        Type: Enum of ButtonType
    """

    def __init__(self, name, code):
        self.name = name
        self.group = "default"
        self.code = code
        self.type = ButtonType.default.name
        #self.can_read = False
        self.value = None
        self.label = None


class Device:
    """ Describes a device. """

    def __init__(self, name):
        self.icon = "null"
        self.name = name
        self.keys = []

    def add_key(self, dev_key):
        """ Adds a new key to the device """
        self.keys.append(dev_key)

    def list_keys(self):
        """ List the keys the device support. """
        return self.keys


def create_key_instance(data):
    """ Create a new key from json data """

    key_instance = Key(data['name'], 0)
    if "group" in data:
        key_instance.group = data['group']
    if "code" in data:
        key_instance.code = data['code']
    if "type" in data:
        key_instance.type = data["type"]
    if "group" in data:
        key_instance.group = data["group"]
    if "label" in data:
        key_instance.label = data["label"]
    if "value" in data:
        key_instance.value = data["value"]

    return key_instance
