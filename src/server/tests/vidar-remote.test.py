import os.path
import sys
import unittest
# import remote_vidar
import readline
from remote_vidar import Completer
from config import Device
from config import Key
sys.path.append(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)),
        os.pardir))


# Here's our "unit tests".
class vidar_remote_test(unittest.TestCase):

    def create_test_devices(self):
        dev_list = []

        test_key = Key("key_test", 42)
        dev = Device("dev_test")
        dev.keys.append(test_key)

        dev_list.append(dev)

    def test_completer_send(self):
        comp = Completer()
        comp = Completer(devices=self.create_test_devices())

        # we want to treat '/' as part of a word, so override the delimiters
        readline.set_completer_delims(' \t\n;')
        readline.parse_and_bind("tab: complete")
        readline.set_completer(comp.complete)

#    def testOne(self):
#        self.failUnless(IsOdd(1))#
#
#
#    def testTwo(self):
#        self.failIf(IsOdd(2))#
#


def main():
    unittest.main()


if __name__ == '__main__':
    main()
