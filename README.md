#  Vidar

Vidar is an universal IR remote which is accessable via a webinterface. 
The name comes from the norse god Vidar which means something like "wide ruler"

This reposistory contains my hardware setup aswell. In my personal setup I'm running it ona a raspberry 3. 
Basically it will running on anything which is capable of running lirc and python3.
<br>
You can view a demo page at https://vidar.mohr.io <br>
Please note that the mobile layout is not working at the momenent under the given url because 
it is embedding the actual page in an IFrame. <br>
To display the mobile layout properly visit http://mohr.io:61505/remote


## Supported Remotes
You can configure your own remote by programming it using the ir receiver.

If your are lucky you can find predefined configuration files here 
http://lirc.sourceforge.net/remotes/

## Installation 
* WARNING you nedd cmake in at least version 3.10 ( raspbian ships with 3.02) * <br>
If you are not familiar with linux please note that commands prefixed with $ are run as your user and commands prefixed # must be run as root.

Be aware that we are using is using submodules, which means you have to clone it like to following:<br> 
`git clone --recursive https://gitlab.com/alexmohr/raspberry-ir-remote`
<br>
<br>
If you forget the `--recursive` in above statement or you are updating an existing clone you need to run this:<br>
`$ git submodule init` <br>
`$ git submodule update` <br>
<br>
<br>
The following steps are only working on systems based on debian. </br>
If the distro is not they might be different and you have to look them up.
* Install dependencies <br></br>
`# apt-get install lirc python3 python3-pip`<br>
<br>
* If you want to use hyperion install these packages aswell <br></br>
`$ sudo apt-get install libffi-dev libssl-dev`<br>
<br>
* Install pyton dependencies <br>
`$ sudo pip3 install bottle cherrypy jsonpickle gevent pluginbase gevent bottle-websocket spur ws4py`
<br>
* Install javascript dependencies <br>
`$ sudo npm install -g less less-plugin-clean-css`
<br>
* Setup init script <br>
`$ sudo ./src/install.sh`
<br>
<br>

## Configration
You can configure your own remote by programming it using the ir receiver or by downloading predefined files from the link bewlow. <br>

http://lirc.sourceforge.net/remotes/
<br>
### Adding own remotes
Please note that configuration files in subfolders are only supported by lirc version >= 0.92. Raspbian ships as of July 2016 with version 0.90 which means all remotes must be in the main config file.
<br>
Large parts of this section have been copied from http://alexba.in/blog/2013/01/06/setting-up-lirc-on-the-raspberrypi/ <br>
<br>
*  Stop lirc to free up the device /dev/lirc0 <br>
`# /etc/init.d/lirc stop`<br>
<br>
* Create a new remote control configuration file (using /dev/lirc0) and save the output to ~/yourDevice.conf <br>
`$ irrecord -d /dev/lirc0 ~/yourDevice.conf`<br>
<br>
* Make a backup of the original lircd.conf file <br>
`$ mv /etc/lirc/lircd.conf /etc/lirc/lircd_original.conf`<br>
<br>
* Extend your configuration file. <br>
`# cat ~/myDevice.conf >> /etc/lirc/lircd.conf` <br>
<br>
* Start up lirc and restart vidar <br>
`# /etc/init.d/lirc start && systemctl restart vidar` <br>
<br>

This approach will not work for remotes communicating in the RC5X protocoll. <br>
To configure a remote using this follow these instructions.<br>

* Download IrScrutinize from http://www.harctoolbox.org/downloads/IrScrutinizer-bin.zip <br>
`mkdir irscrutinizer && cd irscrutinizer && wget http://www.harctoolbox.org/downloads/IrScrutinizer-bin.zip`<br>
<br>
* Download Raspberry pi support
Download the support
`wget https://github.com/bengtmartensson/harctoolboxbundle/releases/download/Version-1.2/raspberry-pi-support.zip`<br>
<br>
* Extract both archives<br>
`unzip IrScrutinizer-bin.zip`<br>
`unzip raspberry-pi-support.zip`<br>
<br>
* Install java<br>
*Warning this will need ~300mb storage*<br>
`# apt-get install openjdk-8-jdk`<br>

<br>
Now you can start the userinterface by running irscrutinizer.sh, if you do not have any kind of graphical userinterface on your pi you could use the discontinued irmaster from harctoolbox. Both tools are pretty must straight foreward. 
As device use mode2 the same way than you test the ir receiver.


## Special Keys
Because I've encountered some missing keys when matching my remotes the following keys have a special icon: 
<table>
    <tr>
        <th>Keycode</th>
        <th>Meaning</th>
        <th>Icon</th>
    </tr>
    <tr>
        <td>KEY_F13</td>
        <td>Blurayplayer</td>
        <td>fontawesome; fa-film; 0xf008</td>
    </tr>
    <tr>
        <td>KEY_F14</td>
        <td>Bluetooth</td>
        <td>fontawesome; fa-bluetooth; 0xf293</td>
    </tr>
    <tr>
        <td>KEY_F15</td>
        <td>USB</td>
        <td>fontawesome; fa-usb; 0xf287</td>
    </tr>
    <tr>
        <td>KEY_F16</td>
        <td>Aux 1</td>
        <td>fontawesome; fa-headphones; 0xf025</td>
    </tr>
    <tr>
        <td>KEY_F17</td>
        <td>Aux 2</td>
        <td>fontawesome; fa-headphones; 0xf025</td>
    </tr>
    <tr>
        <td>KEY_F18</td>
        <td>Internet Radio</td>
        <td>fontawesome; fa-globe; 0xf0ac</td>
    </tr>
    <tr>
        <td>BTN_WHEEL</td>
        <td>Color Wheel</td>
        <td>Color wheel used for the hyperion colour</td>
    </tr>
</table>

 
